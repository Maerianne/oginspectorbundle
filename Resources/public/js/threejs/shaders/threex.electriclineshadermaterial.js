var THREEx	= THREEx	|| {};

THREEx.ElectricLinesShaderMaterial	= function(opts){
	opts	= opts	|| {};
        opts.blending           = opts.blending || THREE.AdditiveBlending;
        opts.transparent        = (typeof opts.transparent !== 'undefined') ? opts.transparent : true;
        opts.depthTest          = (typeof opts.depthTest !== 'undefined') ? opts.depthTest : false;
	opts.vertexShader	= opts.vertexShader	|| THREEx.ElectricLinesShaderMaterial.vertexShader;
	opts.fragmentShader	= opts.fragmentShader	|| THREEx.ElectricLinesShaderMaterial.fragmentShader;
	opts.uniforms		= opts.uniforms || { 
            amplitude: { type: "f", value: 5.0 },
            opacity:   { type: "f", value: 0.3 },
            color:     { type: "c", value: new THREE.Color( 0xff0000 ) }
        };
	var material	= new THREE.ShaderMaterial(opts);
	return material;
};

THREEx.ElectricLinesShaderMaterial.fragmentShader = [

    "uniform vec3 color;",
    "uniform float opacity;",

    "varying vec3 vColor;",

    "void main() {",

        "gl_FragColor = vec4( vColor * color, opacity );",

    "}"

].join("\n");


THREEx.ElectricLinesShaderMaterial.vertexShader = [
    
    "uniform float amplitude;",

    "attribute vec3 displacement;",
    "attribute vec3 customColor;",

    "varying vec3 vColor;",

    "void main() {",

        "vec3 newPosition = position + amplitude * displacement;",

        "vColor = customColor;",

        "gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );",

    "}"

].join("\n");
