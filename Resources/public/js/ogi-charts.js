
$(document).ready(function () { 
    
    Highcharts.setOptions({
        global: {
            getTimezoneOffset: function (timestamp) {
                    var datet = new Date(timestamp);
                    var timezoneOffset = datet.getTimezoneOffset();
                    return timezoneOffset;
                }
        }
    });
    $('.pie-chart').each(function(index, elem){
        var options = {
            chart: {
                renderTo: this,
                type: 'pie',
                backgroundColor:null,
                plotBackgroundColor:null
            },
            title: {
              text: '',  
            },
            xAxis: {
               gridLineColor: '#707073',
               labels: {
                  style: {
                     color: '#E0E0E3'
                  }
               },
               lineColor: '#707073',
               minorGridLineColor: '#505053',
               tickColor: '#707073',
               title: {
                  style: {
                     color: '#A0A0A3'

                  }
               }
            },
            yAxis: {
               gridLineColor: '#707073',
               labels: {
                  style: {
                     color: '#E0E0E3'
                  }
               },
               lineColor: '#707073',
               minorGridLineColor: '#505053',
               tickColor: '#707073',
               tickWidth: 1,
               title: {
                  style: {
                     color: '#A0A0A3'
                  }
               }
            },
            loading: {
                labelStyle: {
                    backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                    display: 'block',
                    width: '136px',
                    height: '26px',
                    backgroundColor: null
                }
            },
            credits: {
                enabled: false,
            },
            tooltip: {
                pointFormat: ' <b>{point.y}</b><br><b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,
                    backgroundColor: null
                },
                series: {
                   dataLabels: {
                      color: '#B0B0B3'
                   },
                   marker: {
                      lineColor: '#333'
                   }
                },
                boxplot: {
                   fillColor: '#505053'
                },
                candlestick: {
                   lineColor: 'white'
                },
                errorbar: {
                   color: 'white'
                }
            },
            legend: {
                itemStyle: {
                   color: '#E0E0E3'
                },
                itemHoverStyle: {
                   color: '#FFF'
                },
                itemHiddenStyle: {
                   color: '#606063'
                }
            },
            series: [{}]
        };
        
        $.ajax({
            type: "GET",
            url: $(this).attr('data'),
            beforeSend: function(xhr) {
              xhr.setRequestHeader("X-Requested-With", 'XMLHttpRequest'); 
            },
            xhrFields: {
              withCredentials: true
            },
            processData: true,
            crossDomain: true,
            async: true,
            contentType: 'application/json',
            dataType: 'json',
            fail:function(jqXHR, textStatus, errorThrown){
                //alert(errorThrown);
            }
        }).done(function(data){
            options.series = data;
            var chart = new Highcharts.Chart(options);
        });
        
//        $.getJSON($(this).attr('data')).done(function(data){
//            options.series = data;
//            var chart = new Highcharts.Chart(options);
//        }).fail(function(){
//            var chart = new Highcharts.Chart(options);
//        });
    });
    
    $('.stock-chart').each(function(){
        var options = {
            chart: {
                renderTo: this,
                type: 'spline'
            },
            title: {
              text: '',  
            },
            loading: {
                labelStyle: {
                    backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                    display: 'block',
                    width: '136px',
                    height: '26px',
                    backgroundColor: '#000'
                }
            },
            credits: {
                enabled: false,
            },
            legend: {
                enabled: true,
            },
            tooltip: {
                enabled: true,  
            },
            series: [{}]
        };
       
        $.getJSON($(this).attr('data')).done(function(data){
            options.series = data;
            var chart = new Highcharts.StockChart(options);
        }).fail(function(){
            var chart = new Highcharts.StockChart(options);
        });
    });
    
    $('.chart').each(function(){
        var options = {
            chart: {
                renderTo: this,
                type: 'spline'
            },
            title: {
              text: '',  
            },
            xAxis: {
                type: 'datetime',
            },
            loading: {
                labelStyle: {
                    backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                    display: 'block',
                    width: '136px',
                    height: '26px',
                    backgroundColor: '#000'
                }
            },
            credits: {
                enabled: false,
            },
            tooltip: {
                shared: true,
                crosshairs: true
            },
            series: [{}]
        };
       
        $.getJSON($(this).attr('data')).done(function(data){
            options.series = data;
            var chart = new Highcharts.Chart(options);
        }).fail(function(){
            var chart = new Highcharts.Chart(options);
            chart.showLoading();
        });
    });
    
    $('.scatter-chart').each(function(){
        var options = {
            chart: {
                renderTo: this,
                type: 'scatter'
            },
            title: {
              text: '',  
            },
            xAxis: {
                min: 1,
                max: 499
                //type: 'datetime',
            },
            yAxis: {
                min: 1,
                max: 15
                //type: 'datetime',
            },
            loading: {
                labelStyle: {
                    backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                    display: 'block',
                    width: '136px',
                    height: '26px',
                    backgroundColor: '#000'
                }
            },
            credits: {
                enabled: false,
            },
            tooltip: {
                shared: true,
                crosshairs: true
            },
            series: [{}]
        };
        
        $.getJSON($(this).attr('data')).done(function(data){
            options.series = data;
            var chart = new Highcharts.Chart(options);
        }).fail(function(){
            var chart = new Highcharts.Chart(options);
            chart.showLoading();
        });
    });
    
    $('.heatmap').each(function(){
        var options = {
            chart: {
                renderTo: this,
                type: 'heatmap'
            },
            title: {
              text: '',  
            },
            xAxis: {
                //type: 'datetime',
            },
            
            colorAxis: {
                stops: [
                    [0, '#3060cf'],
                    [0.5, '#fffbbc'],
                    [0.9, '#c4463a'],
                    [1, '#c4463a']
                ],
                startOnTick: false,
                endOnTick: false,
                labels: {
                    format: '{value}℃'
                }
            },
            
            loading: {
                labelStyle: {
                    backgroundImage: 'url("http://jsfiddle.net/img/logo.png")',
                    display: 'block',
                    width: '136px',
                    height: '26px',
                    backgroundColor: '#000'
                }
            },
            credits: {
                enabled: false,
            },
            /*tooltip: {
                shared: true,
                crosshairs: true
            },*/
            series: [{}]
        };
        
        $.getJSON($(this).attr('data')).done(function(data){
            options.series = data;
            var chart = new Highcharts.Chart(options);
        }).fail(function(){
            var chart = new Highcharts.Chart(options);
            chart.showLoading();
        });
    });
});