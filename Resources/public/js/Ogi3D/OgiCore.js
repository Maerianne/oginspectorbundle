
OgiCore = function(container, parameters){
    this.container = container;
    if(typeof parameters !== "undefined"){
        
    }
    this.statsEnabled = (typeof parameters !== 'undefined' && typeof parameters.statsEnabled !== 'undefined') ? parameters.statsEnabled : false;
    
    this.initialized = false;
    
    
};

OgiCore.prototype = {
    constructor: OgiCore,
    
    eventManager: new EventHandler(),
    
    scene: new THREE.Scene(),
    
    loader: {
        manager: new THREE.LoadingManager()
    },
    
    elements: [],
    
    ressources: {
        images: [],
        textures: [],
        models: [],
        
        init: function(){
            
            
        },
        
        get: function(path){
            
            this.textures.forEach(function(item){
                if(item.path === path){
                    return item;
                }
            });

            return null;
            
        },
        
        add: function(item){
            if(typeof item.image !== "undefined"){
                if(isArray(item.image)){
                    item.image.forEach(function(item){
                        
                    });
                }
            }
            
        }
        
    },
    
    
    
    init: function(){
        var context = this;
        
        this.initListeners(function(){ 
            context.eventManager.fire({ type: 'init-core',target: context});
        });
        
        $(window).resize(function(){
            context.onWindowResize();
        });
//        this.scene.fog =
        this.initialized = true;
    },
    
    initListeners: function(callback){
        var context = this;
        
        this.eventManager.addListener('*', function(event){
            console.log(event);
        });
        
        this.eventManager.addListener('init-core', function(event){
            context.eventManager.fire({ type: 'init-loaders',target: context});
        });
        
        this.eventManager.addListener('init-loaders', function(event){
            context.initLoaders(function(){
                context.eventManager.fire({ type: 'loaders-initialized',target: context});
            });
        });
        
        this.eventManager.addListener('loaders-initialized', function(event){
            context.eventManager.fire({ type: 'init-renderer',target: context});
        });
        
        this.eventManager.addListener('init-renderer', function(event){
            context.initRenderer(function(){
                context.eventManager.fire({ type: 'renderer-initialized',target: context});
            });
        });
        
        this.eventManager.addListener('renderer-initialized', function(event){
            context.eventManager.fire({ type: 'init-camera',target: context});
        });
        
        this.eventManager.addListener('init-camera', function(event){
            context.initCamera(function(){
                context.eventManager.fire({ type: 'camera-initialized',target: context});
            });
        });
        
        this.eventManager.addListener('camera-initialized', function(event){
            context.eventManager.fire({ type: 'init-scene',target: context});
        });
        
        this.eventManager.addListener('init-scene', function(event){
            context.initScene(function(){
                context.eventManager.fire({ type: 'scene-initialized',target: context});
            });
        });
        
        this.eventManager.addListener('scene-initialized', function(event){
            context.eventManager.fire({ type: 'init-extensions',target: context});
        });
        
        this.eventManager.addListener('init-extensions', function(event){
            if(context.statsEnabled){
                context.initStats();
            }
        });
        
        callback();
    },
    
    initLoaders: function(callback){
        
        this.loader.image = new THREE.ImageLoader(this.loader.manager);
        this.loader.texture = new THREE.TextureLoader(this.loader.manager);   
        
        this.loader.manager.onProgress = function ( item, loaded, total ) {
            console.log( "item: "+item, "loaded: "+loaded, "total: "+total );
        };
        this.loader.manager.onLoad = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        this.loader.manager.onError = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };

        this.loader.image.onProgress = function ( item, loaded, total ) {
            console.log( "item: "+item, "loaded: "+loaded, "total: "+total );
        };
        this.loader.image.onLoad = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        this.loader.image.onError = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };

        this.loader.texture.onProgress = function ( item, loaded, total ) {
            console.log( "item: "+item, "loaded: "+loaded, "total: "+total );
        };
        this.loader.texture.onLoad = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        this.loader.texture.onError = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };

        callback();
    },
    
    getTexture: function(path){
        this.ressources.textures.forEach(function(item){
            if(item.path === path){
                return item;
            }
        });
        
        return null;
    },
    
    addTexture: function(path){
        if(this.getTexture(path) !== null){
            this.ressources.textures.push({ path: path, ressource: null});
        }
        
    },
    
    getImage: function(path){
        this.ressources.images.forEach(function(item){
            if(item.path === path){
                return item;
            }
        });
        
        return null;
    },
    
    addImage: function(path){
        if(this.getImage(path) !== null){
            this.ressources.images.push({ path: path, ressource: null});
        }
        
    },
    
    getModel: function(path){
        this.ressources.models.forEach(function(item){
            if(item.path === path){
                return item;
            }
        });
        
        return null;
    },
    
    addModel: function(path){
        if(this.getModel(path) !== null){
            this.ressources.models.push({ path: path, ressource: null});
        }
        
    },
    
    
    
    
    loadRessources: function(){
        var context = this;
        
        this.ressources.images.forEach(function(item){
            context.loader.image.load(item.path, function(image){
                item.ressource = image;
            });
        });
        
        this.ressources.textures.forEach(function(item){
            context.loader.texture.load(item.path, function(texture){
                item.ressource = texture;
            });
        });
    },
    
    initScene: function(callback){
        this.initLight();
        //this.initSkybox();
        callback();
    },
    
    addSkybox: function(path){
        var cubeMap = new THREE.CubeTexture( [] );
	cubeMap.format = THREE.RGBFormat;
	cubeMap.flipY = false;
	this.loader.image.load( path, function ( image ) {
                var getSide = function ( x, y ) {
                    var size = 1024;
                    var canvas = document.createElement( 'canvas' );
                    canvas.width = size;
                    canvas.height = size;
                    var context = canvas.getContext( '2d' );
                    context.drawImage( image, - x * size, - y * size );
                    return canvas;
		};
                
		cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
		cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
		cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
		cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
		cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
		cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
		cubeMap.needsUpdate = true;
	} );
	
        var cubeShader = THREEx.SkyShaderMaterial();
	cubeShader.uniforms['tCube'].value = cubeMap;
                                
                                
        var skyBoxMaterial = new THREE.ShaderMaterial( {
                fragmentShader: cubeShader.fragmentShader,
                vertexShader: cubeShader.vertexShader,
                uniforms: cubeShader.uniforms,
                depthWrite: true,
                side: THREE.BackSide
        });
                                
	this.skyBox = new THREE.Mesh(
            new THREE.BoxGeometry(1000000, 1000000, 1000000 ),
            skyBoxMaterial
        );
				
	this.scene.add( this.skyBox );
        
    },
    
    
    
    initLight: function(){
        this.directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
	this.directionalLight.position.set(0, 0, 0 ).normalize();
              
        this.directionalLight.shadowCameraNear = 2;
	this.directionalLight.shadowCameraFar = 8192;
	this.directionalLight.shadowCameraFov = 8192;
        
        this.directionalLight.castShadow = true;
        this.directionalLight.shadowBias = 0.000001;
	this.directionalLight.shadowDarkness = 0.9;
        this.directionalLight.shadowMapWidth = 8192;
	this.directionalLight.shadowMapHeight = 8192;
        
//        this.ambientLight = new THREE.AmbientLight( 0x101030 );
//        this.scene.add( this.ambientLight );
        
	this.scene.add( this.directionalLight );
    },
    
    initCamera: function(callback){
        this.camera = new THREE.PerspectiveCamera(60, this.container.width() / this.container.height(), 1, 1000000);
        this.camera.position.set( 0, 0, 0 );
        this.camera.lookAt(new THREE.Vector3(0,0,0));
        callback();
    },
    
    initRenderer: function(callback){
        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( this.container.width(), this.container.height() );
        this.renderer.setClearColor( 0x777777 );
//        this.renderer.setClearColor( 0x050505 );
        this.renderer.autoClear = true;
        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapType = THREE.PCFShadowMap;
        this.container.append( this.renderer.domElement );
        
        callback();
    },
    
    initStats: function(){
        this.stats = new Stats();
        
        this.stats.domElement.style.position = 'relative';
        this.stats.domElement.style.top = '0px';
        this.container.append( this.stats.domElement );
    },
    
    addElement: function(element){
        this.elements.push(element);
        this.scene.add(element.getMesh());
    },
        
    animate: function() {
        var context = this;
        
        window.requestAnimationFrame( function(){ 
            context.animate(); 
        });
        
        if(typeof this.scene !== "undefined" && this.initialized){
            this.elements.forEach (function(item) {
                item.animate();
            });
            
            if(this.statsEnabled){
                this.stats.update();
            }
            
            this.render();
        }
    },

    render: function() {
        this.renderer.render( this.scene, this.camera );
    },
    
    onWindowResize: function(){
        this.camera.aspect = this.container.width()/ this.container.height();
	this.camera.updateProjectionMatrix();
        this.renderer.setSize( this.container.width(), this.container.height() );
    }
};