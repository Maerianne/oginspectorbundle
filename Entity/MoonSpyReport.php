<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * MoonSpyReport
 * 
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\MoonSpyReportRepository")
 * 
 */
class MoonSpyReport extends SpyReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Lune
     * @ORM\ManyToOne(targetEntity="Maesbox\OGInspectorBundle\Entity\Lune",inversedBy="spy_reports" )
     * @ORM\JoinColumn(name="lune_id", referencedColumnName="id", nullable=true)
     **/
    protected $lune;
    
    /**
     * LUNES INSTALLATIONS
     */
    
    /**
     * @var integer
     * @ORM\Column(name="lunar_base", type="integer", nullable=true)
     */
    protected $lunar_base;
    
    /**
     * @var integer
     * @ORM\Column(name="captors_phalange", type="integer", nullable=true)
     */
    protected $captors_phalange;
    
    /**
     * @var integer
     * @ORM\Column(name="stargate", type="integer", nullable=true)
     */
    protected $stargate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lunarBase
     *
     * @param integer $lunarBase
     *
     * @return MoonSpyReport
     */
    public function setLunarBase($lunarBase)
    {
        $this->lunar_base = $lunarBase;

        return $this;
    }

    /**
     * Get lunarBase
     *
     * @return integer
     */
    public function getLunarBase()
    {
        return $this->lunar_base;
    }

    /**
     * Set captorsPhalange
     *
     * @param integer $captorsPhalange
     *
     * @return MoonSpyReport
     */
    public function setCaptorsPhalange($captorsPhalange)
    {
        $this->captors_phalange = $captorsPhalange;

        return $this;
    }

    /**
     * Get captorsPhalange
     *
     * @return integer
     */
    public function getCaptorsPhalange()
    {
        return $this->captors_phalange;
    }

    /**
     * Set stargate
     *
     * @param integer $stargate
     *
     * @return MoonSpyReport
     */
    public function setStargate($stargate)
    {
        $this->stargate = $stargate;

        return $this;
    }

    /**
     * Get stargate
     *
     * @return integer
     */
    public function getStargate()
    {
        return $this->stargate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MoonSpyReport
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set metal
     *
     * @param integer $metal
     *
     * @return MoonSpyReport
     */
    public function setMetal($metal)
    {
        $this->metal = $metal;

        return $this;
    }

    /**
     * Get metal
     *
     * @return integer
     */
    public function getMetal()
    {
        return $this->metal;
    }

    /**
     * Set cristal
     *
     * @param integer $cristal
     *
     * @return MoonSpyReport
     */
    public function setCristal($cristal)
    {
        $this->cristal = $cristal;

        return $this;
    }

    /**
     * Get cristal
     *
     * @return integer
     */
    public function getCristal()
    {
        return $this->cristal;
    }

    /**
     * Set deuterium
     *
     * @param integer $deuterium
     *
     * @return MoonSpyReport
     */
    public function setDeuterium($deuterium)
    {
        $this->deuterium = $deuterium;

        return $this;
    }

    /**
     * Get deuterium
     *
     * @return integer
     */
    public function getDeuterium()
    {
        return $this->deuterium;
    }

    /**
     * Set robotFactory
     *
     * @param integer $robotFactory
     *
     * @return MoonSpyReport
     */
    public function setRobotFactory($robotFactory)
    {
        $this->robot_factory = $robotFactory;

        return $this;
    }

    /**
     * Get robotFactory
     *
     * @return integer
     */
    public function getRobotFactory()
    {
        return $this->robot_factory;
    }

    /**
     * Set spaceFactory
     *
     * @param integer $spaceFactory
     *
     * @return MoonSpyReport
     */
    public function setSpaceFactory($spaceFactory)
    {
        $this->space_factory = $spaceFactory;

        return $this;
    }

    /**
     * Get spaceFactory
     *
     * @return integer
     */
    public function getSpaceFactory()
    {
        return $this->space_factory;
    }

    /**
     * Set energyTechnology
     *
     * @param integer $energyTechnology
     *
     * @return MoonSpyReport
     */
    public function setEnergyTechnology($energyTechnology)
    {
        $this->energy_technology = $energyTechnology;

        return $this;
    }

    /**
     * Get energyTechnology
     *
     * @return integer
     */
    public function getEnergyTechnology()
    {
        return $this->energy_technology;
    }

    /**
     * Set laserTechnology
     *
     * @param integer $laserTechnology
     *
     * @return MoonSpyReport
     */
    public function setLaserTechnology($laserTechnology)
    {
        $this->laser_technology = $laserTechnology;

        return $this;
    }

    /**
     * Get laserTechnology
     *
     * @return integer
     */
    public function getLaserTechnology()
    {
        return $this->laser_technology;
    }

    /**
     * Set ionTechnology
     *
     * @param integer $ionTechnology
     *
     * @return MoonSpyReport
     */
    public function setIonTechnology($ionTechnology)
    {
        $this->ion_technology = $ionTechnology;

        return $this;
    }

    /**
     * Get ionTechnology
     *
     * @return integer
     */
    public function getIonTechnology()
    {
        return $this->ion_technology;
    }

    /**
     * Set plasmaTechnology
     *
     * @param integer $plasmaTechnology
     *
     * @return MoonSpyReport
     */
    public function setPlasmaTechnology($plasmaTechnology)
    {
        $this->plasma_technology = $plasmaTechnology;

        return $this;
    }

    /**
     * Get plasmaTechnology
     *
     * @return integer
     */
    public function getPlasmaTechnology()
    {
        return $this->plasma_technology;
    }

    /**
     * Set hyperspaceTechnology
     *
     * @param integer $hyperspaceTechnology
     *
     * @return MoonSpyReport
     */
    public function setHyperspaceTechnology($hyperspaceTechnology)
    {
        $this->hyperspace_technology = $hyperspaceTechnology;

        return $this;
    }

    /**
     * Get hyperspaceTechnology
     *
     * @return integer
     */
    public function getHyperspaceTechnology()
    {
        return $this->hyperspace_technology;
    }

    /**
     * Set combustionPropulsionTechnology
     *
     * @param integer $combustionPropulsionTechnology
     *
     * @return MoonSpyReport
     */
    public function setCombustionPropulsionTechnology($combustionPropulsionTechnology)
    {
        $this->combustion_propulsion_technology = $combustionPropulsionTechnology;

        return $this;
    }

    /**
     * Get combustionPropulsionTechnology
     *
     * @return integer
     */
    public function getCombustionPropulsionTechnology()
    {
        return $this->combustion_propulsion_technology;
    }

    /**
     * Set impulsionPropulsionTechnology
     *
     * @param integer $impulsionPropulsionTechnology
     *
     * @return MoonSpyReport
     */
    public function setImpulsionPropulsionTechnology($impulsionPropulsionTechnology)
    {
        $this->impulsion_propulsion_technology = $impulsionPropulsionTechnology;

        return $this;
    }

    /**
     * Get impulsionPropulsionTechnology
     *
     * @return integer
     */
    public function getImpulsionPropulsionTechnology()
    {
        return $this->impulsion_propulsion_technology;
    }

    /**
     * Set hyperspacePropulsionTechnology
     *
     * @param integer $hyperspacePropulsionTechnology
     *
     * @return MoonSpyReport
     */
    public function setHyperspacePropulsionTechnology($hyperspacePropulsionTechnology)
    {
        $this->hyperspace_propulsion_technology = $hyperspacePropulsionTechnology;

        return $this;
    }

    /**
     * Get hyperspacePropulsionTechnology
     *
     * @return integer
     */
    public function getHyperspacePropulsionTechnology()
    {
        return $this->hyperspace_propulsion_technology;
    }

    /**
     * Set spyTechnology
     *
     * @param integer $spyTechnology
     *
     * @return MoonSpyReport
     */
    public function setSpyTechnology($spyTechnology)
    {
        $this->spy_technology = $spyTechnology;

        return $this;
    }

    /**
     * Get spyTechnology
     *
     * @return integer
     */
    public function getSpyTechnology()
    {
        return $this->spy_technology;
    }

    /**
     * Set computerTechnology
     *
     * @param integer $computerTechnology
     *
     * @return MoonSpyReport
     */
    public function setComputerTechnology($computerTechnology)
    {
        $this->computer_technology = $computerTechnology;

        return $this;
    }

    /**
     * Get computerTechnology
     *
     * @return integer
     */
    public function getComputerTechnology()
    {
        return $this->computer_technology;
    }

    /**
     * Set astrophysicTechnology
     *
     * @param integer $astrophysicTechnology
     *
     * @return MoonSpyReport
     */
    public function setAstrophysicTechnology($astrophysicTechnology)
    {
        $this->astrophysic_technology = $astrophysicTechnology;

        return $this;
    }

    /**
     * Get astrophysicTechnology
     *
     * @return integer
     */
    public function getAstrophysicTechnology()
    {
        return $this->astrophysic_technology;
    }

    /**
     * Set researchNetworkTechnology
     *
     * @param integer $researchNetworkTechnology
     *
     * @return MoonSpyReport
     */
    public function setResearchNetworkTechnology($researchNetworkTechnology)
    {
        $this->research_network_technology = $researchNetworkTechnology;

        return $this;
    }

    /**
     * Get researchNetworkTechnology
     *
     * @return integer
     */
    public function getResearchNetworkTechnology()
    {
        return $this->research_network_technology;
    }

    /**
     * Set gravitonTechnology
     *
     * @param integer $gravitonTechnology
     *
     * @return MoonSpyReport
     */
    public function setGravitonTechnology($gravitonTechnology)
    {
        $this->graviton_technology = $gravitonTechnology;

        return $this;
    }

    /**
     * Get gravitonTechnology
     *
     * @return integer
     */
    public function getGravitonTechnology()
    {
        return $this->graviton_technology;
    }

    /**
     * Set weaponTechnology
     *
     * @param integer $weaponTechnology
     *
     * @return MoonSpyReport
     */
    public function setWeaponTechnology($weaponTechnology)
    {
        $this->weapon_technology = $weaponTechnology;

        return $this;
    }

    /**
     * Get weaponTechnology
     *
     * @return integer
     */
    public function getWeaponTechnology()
    {
        return $this->weapon_technology;
    }

    /**
     * Set shieldTechnology
     *
     * @param integer $shieldTechnology
     *
     * @return MoonSpyReport
     */
    public function setShieldTechnology($shieldTechnology)
    {
        $this->shield_technology = $shieldTechnology;

        return $this;
    }

    /**
     * Get shieldTechnology
     *
     * @return integer
     */
    public function getShieldTechnology()
    {
        return $this->shield_technology;
    }

    /**
     * Set structureTechnology
     *
     * @param integer $structureTechnology
     *
     * @return MoonSpyReport
     */
    public function setStructureTechnology($structureTechnology)
    {
        $this->structure_technology = $structureTechnology;

        return $this;
    }

    /**
     * Get structureTechnology
     *
     * @return integer
     */
    public function getStructureTechnology()
    {
        return $this->structure_technology;
    }

    /**
     * Set missileLauncher
     *
     * @param integer $missileLauncher
     *
     * @return MoonSpyReport
     */
    public function setMissileLauncher($missileLauncher)
    {
        $this->missile_launcher = $missileLauncher;

        return $this;
    }

    /**
     * Get missileLauncher
     *
     * @return integer
     */
    public function getMissileLauncher()
    {
        return $this->missile_launcher;
    }

    /**
     * Set lightLaser
     *
     * @param integer $lightLaser
     *
     * @return MoonSpyReport
     */
    public function setLightLaser($lightLaser)
    {
        $this->light_laser = $lightLaser;

        return $this;
    }

    /**
     * Get lightLaser
     *
     * @return integer
     */
    public function getLightLaser()
    {
        return $this->light_laser;
    }

    /**
     * Set heavyLaser
     *
     * @param integer $heavyLaser
     *
     * @return MoonSpyReport
     */
    public function setHeavyLaser($heavyLaser)
    {
        $this->heavy_laser = $heavyLaser;

        return $this;
    }

    /**
     * Get heavyLaser
     *
     * @return integer
     */
    public function getHeavyLaser()
    {
        return $this->heavy_laser;
    }

    /**
     * Set ionArtillery
     *
     * @param integer $ionArtillery
     *
     * @return MoonSpyReport
     */
    public function setIonArtillery($ionArtillery)
    {
        $this->ion_artillery = $ionArtillery;

        return $this;
    }

    /**
     * Get ionArtillery
     *
     * @return integer
     */
    public function getIonArtillery()
    {
        return $this->ion_artillery;
    }

    /**
     * Set gaussCanon
     *
     * @param integer $gaussCanon
     *
     * @return MoonSpyReport
     */
    public function setGaussCanon($gaussCanon)
    {
        $this->gauss_canon = $gaussCanon;

        return $this;
    }

    /**
     * Get gaussCanon
     *
     * @return integer
     */
    public function getGaussCanon()
    {
        return $this->gauss_canon;
    }

    /**
     * Set plasmaLauncher
     *
     * @param integer $plasmaLauncher
     *
     * @return MoonSpyReport
     */
    public function setPlasmaLauncher($plasmaLauncher)
    {
        $this->plasma_launcher = $plasmaLauncher;

        return $this;
    }

    /**
     * Get plasmaLauncher
     *
     * @return integer
     */
    public function getPlasmaLauncher()
    {
        return $this->plasma_launcher;
    }

    /**
     * Set satellites
     *
     * @param integer $satellites
     *
     * @return MoonSpyReport
     */
    public function setSatellites($satellites)
    {
        $this->satellites = $satellites;

        return $this;
    }

    /**
     * Get satellites
     *
     * @return integer
     */
    public function getSatellites()
    {
        return $this->satellites;
    }

    /**
     * Set lightShield
     *
     * @param boolean $lightShield
     *
     * @return MoonSpyReport
     */
    public function setLightShield($lightShield)
    {
        $this->light_shield = $lightShield;

        return $this;
    }

    /**
     * Get lightShield
     *
     * @return boolean
     */
    public function getLightShield()
    {
        return $this->light_shield;
    }

    /**
     * Set heavyShield
     *
     * @param boolean $heavyShield
     *
     * @return MoonSpyReport
     */
    public function setHeavyShield($heavyShield)
    {
        $this->heavy_shield = $heavyShield;

        return $this;
    }

    /**
     * Get heavyShield
     *
     * @return boolean
     */
    public function getHeavyShield()
    {
        return $this->heavy_shield;
    }

    /**
     * Set lightFighter
     *
     * @param integer $lightFighter
     *
     * @return MoonSpyReport
     */
    public function setLightFighter($lightFighter)
    {
        $this->light_fighter = $lightFighter;

        return $this;
    }

    /**
     * Get lightFighter
     *
     * @return integer
     */
    public function getLightFighter()
    {
        return $this->light_fighter;
    }

    /**
     * Set heavyFighter
     *
     * @param integer $heavyFighter
     *
     * @return MoonSpyReport
     */
    public function setHeavyFighter($heavyFighter)
    {
        $this->heavy_fighter = $heavyFighter;

        return $this;
    }

    /**
     * Get heavyFighter
     *
     * @return integer
     */
    public function getHeavyFighter()
    {
        return $this->heavy_fighter;
    }

    /**
     * Set cruiser
     *
     * @param integer $cruiser
     *
     * @return MoonSpyReport
     */
    public function setCruiser($cruiser)
    {
        $this->cruiser = $cruiser;

        return $this;
    }

    /**
     * Get cruiser
     *
     * @return integer
     */
    public function getCruiser()
    {
        return $this->cruiser;
    }

    /**
     * Set battleship
     *
     * @param integer $battleship
     *
     * @return MoonSpyReport
     */
    public function setBattleship($battleship)
    {
        $this->battleship = $battleship;

        return $this;
    }

    /**
     * Get battleship
     *
     * @return integer
     */
    public function getBattleship()
    {
        return $this->battleship;
    }

    /**
     * Set tracker
     *
     * @param integer $tracker
     *
     * @return MoonSpyReport
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;

        return $this;
    }

    /**
     * Get tracker
     *
     * @return integer
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set bomber
     *
     * @param integer $bomber
     *
     * @return MoonSpyReport
     */
    public function setBomber($bomber)
    {
        $this->bomber = $bomber;

        return $this;
    }

    /**
     * Get bomber
     *
     * @return integer
     */
    public function getBomber()
    {
        return $this->bomber;
    }

    /**
     * Set destroyer
     *
     * @param integer $destroyer
     *
     * @return MoonSpyReport
     */
    public function setDestroyer($destroyer)
    {
        $this->destroyer = $destroyer;

        return $this;
    }

    /**
     * Get destroyer
     *
     * @return integer
     */
    public function getDestroyer()
    {
        return $this->destroyer;
    }

    /**
     * Set deathStar
     *
     * @param integer $deathStar
     *
     * @return MoonSpyReport
     */
    public function setDeathStar($deathStar)
    {
        $this->death_star = $deathStar;

        return $this;
    }

    /**
     * Get deathStar
     *
     * @return integer
     */
    public function getDeathStar()
    {
        return $this->death_star;
    }

    /**
     * Set recycler
     *
     * @param integer $recycler
     *
     * @return MoonSpyReport
     */
    public function setRecycler($recycler)
    {
        $this->recycler = $recycler;

        return $this;
    }

    /**
     * Get recycler
     *
     * @return integer
     */
    public function getRecycler()
    {
        return $this->recycler;
    }

    /**
     * Set lightTransporter
     *
     * @param integer $lightTransporter
     *
     * @return MoonSpyReport
     */
    public function setLightTransporter($lightTransporter)
    {
        $this->light_transporter = $lightTransporter;

        return $this;
    }

    /**
     * Get lightTransporter
     *
     * @return integer
     */
    public function getLightTransporter()
    {
        return $this->light_transporter;
    }

    /**
     * Set heavyTransporter
     *
     * @param integer $heavyTransporter
     *
     * @return MoonSpyReport
     */
    public function setHeavyTransporter($heavyTransporter)
    {
        $this->heavy_transporter = $heavyTransporter;

        return $this;
    }

    /**
     * Get heavyTransporter
     *
     * @return integer
     */
    public function getHeavyTransporter()
    {
        return $this->heavy_transporter;
    }

    /**
     * Set spyProbe
     *
     * @param integer $spyProbe
     *
     * @return MoonSpyReport
     */
    public function setSpyProbe($spyProbe)
    {
        $this->spy_probe = $spyProbe;

        return $this;
    }

    /**
     * Get spyProbe
     *
     * @return integer
     */
    public function getSpyProbe()
    {
        return $this->spy_probe;
    }

    /**
     * Set colonisationShip
     *
     * @param integer $colonisationShip
     *
     * @return MoonSpyReport
     */
    public function setColonisationShip($colonisationShip)
    {
        $this->colonisation_ship = $colonisationShip;

        return $this;
    }

    /**
     * Get colonisationShip
     *
     * @return integer
     */
    public function getColonisationShip()
    {
        return $this->colonisation_ship;
    }

    /**
     * Set lune
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Lune $lune
     *
     * @return MoonSpyReport
     */
    public function setLune(\Maesbox\OGInspectorBundle\Entity\Lune $lune = null)
    {
        $this->lune = $lune;

        return $this;
    }

    /**
     * Get lune
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Lune
     */
    public function getLune()
    {
        return $this->lune;
    }

    /**
     * Set player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     *
     * @return MoonSpyReport
     */
    public function setPlayer(\Maesbox\OGInspectorBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set target
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $target
     *
     * @return MoonSpyReport
     */
    public function setTarget(\Maesbox\OGInspectorBundle\Entity\Player $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Player
     */
    public function getTarget()
    {
        return $this->target;
    }
}
