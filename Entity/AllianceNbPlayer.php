<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Planete
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\AllianceNbPlayerRepository")
 */
class AllianceNbPlayer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Alliance
     * @ORM\ManyToOne(targetEntity="Alliance", inversedBy="nb_player")
     * @ORM\JoinColumn(name="alliance_id", referencedColumnName="id")
     */
    protected $alliance;
    
    /**
     * @var Datetime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var integer
     * @ORM\Column(name="nb_player", type="integer")
     */
    protected $nb_player;
        
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AllianceNbPlayer
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set nbPlayer
     *
     * @param integer $nbPlayer
     *
     * @return AllianceNbPlayer
     */
    public function setNbPlayer($nbPlayer)
    {
        $this->nb_player = $nbPlayer;

        return $this;
    }

    /**
     * Get nbPlayer
     *
     * @return integer
     */
    public function getNbPlayer()
    {
        return $this->nb_player;
    }

    /**
     * Set alliance
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Alliance $alliance
     *
     * @return AllianceNbPlayer
     */
    public function setAlliance(\Maesbox\OGInspectorBundle\Entity\Alliance $alliance = null)
    {
        $this->alliance = $alliance;

        return $this;
    }

    /**
     * Get alliance
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Alliance
     */
    public function getAlliance()
    {
        return $this->alliance;
    }
}
