<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Maesbox\OGInspectorBundle\Validator as MaesboxAssert;

/**
 * Univers
 * 
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\UniversRepository")
 * 
 * @MaesboxAssert\UniversValidator
 */
class Univers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
        
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;
    
    /**
     * @var integer
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    protected $number;
    
    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable=true)
     */
    protected $language;
    
    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", nullable=true, unique=true)
     */
    protected $domain;
    
    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", nullable=true)
     */
    protected $version;
    
    /**
     * @var integer
     * @ORM\Column(name="speed", type="float")
     */
    protected $speed;
    
    /**
     * @var integer
     * @ORM\Column(name="fleet_speed", type="float")
     */
    protected $fleet_speed;
    
    /**
     * @var boolean 
     * @ORM\Column(name="acs", type="boolean")
     */
    protected $acs;
    
    /**
     * @var boolean 
     * @ORM\Column(name="rapid_fire", type="boolean")
     */
    protected $rapid_fire;
    
    /**
     * @var integer
     * @ORM\Column(name="debris_factor", type="float")
     */
    protected $debris_factor;
    
    /**
     * @var integer
     * @ORM\Column(name="repair_factor", type="float")
     */
    protected $repair_factor;
    
    /**
     * @var integer
     * @ORM\Column(name="newbie_protection_limit", type="integer")
     */
    protected $newbie_protection_limit;
    
    /**
     * @var integer
     * @ORM\Column(name="newbie_protection_high", type="integer")
     */
    protected $newbie_protection_high;
    
    /**
     * @var integer
     * @ORM\Column(name="bonus_fields", type="integer")
     */
    protected $bonus_fields;

    /**
     * @var integer
     * @ORM\Column(name="nb_galaxys", type="integer")
     */
    protected $nb_galaxys;
    
    /**
     * @var integer
     * @ORM\Column(name="nb_systems", type="integer")
     */
    protected $nb_systems;
    
    /**
     * @var integer
     * @ORM\Column(name="nb_planetes", type="integer")
     */
    protected $nb_planetes = 15;

    /**
     * @var boolean 
     * @ORM\Column(name="donut_galaxy", type="boolean")
     */
    protected $donut_galaxy;
    
    /**
     * @var boolean 
     * @ORM\Column(name="donut_system", type="boolean")
     */
    protected $donut_system;
    
    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="univers")
     **/
    protected $players;
    
    /**
     * @ORM\OneToMany(targetEntity="Alliance", mappedBy="univers")
     **/
    protected $alliances;
    
    /**
     * @ORM\OneToMany(targetEntity="Planete", mappedBy="univers", cascade={"persist","remove"})
     **/
    protected $planetes;
    
    
    /**
     * @ORM\PrePersist
     */
    public function setDomainValue()
    {    
        $this->domain = "http://s".$this->getNumber()."-".$this->getLanguage().".ogame.gameforge.com";
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->alliances = new ArrayCollection();
        $this->planetes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Univers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Univers
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Univers
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Univers
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return Univers
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set speed
     *
     * @param float $speed
     *
     * @return Univers
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set fleetSpeed
     *
     * @param float $fleetSpeed
     *
     * @return Univers
     */
    public function setFleetSpeed($fleetSpeed)
    {
        $this->fleet_speed = $fleetSpeed;

        return $this;
    }

    /**
     * Get fleetSpeed
     *
     * @return float
     */
    public function getFleetSpeed()
    {
        return $this->fleet_speed;
    }

    /**
     * Set acs
     *
     * @param boolean $acs
     *
     * @return Univers
     */
    public function setAcs($acs)
    {
        $this->acs = $acs;

        return $this;
    }

    /**
     * Get acs
     *
     * @return boolean
     */
    public function getAcs()
    {
        return $this->acs;
    }

    /**
     * Set rapidFire
     *
     * @param boolean $rapidFire
     *
     * @return Univers
     */
    public function setRapidFire($rapidFire)
    {
        $this->rapid_fire = $rapidFire;

        return $this;
    }

    /**
     * Get rapidFire
     *
     * @return boolean
     */
    public function getRapidFire()
    {
        return $this->rapid_fire;
    }

    /**
     * Set debrisFactor
     *
     * @param float $debrisFactor
     *
     * @return Univers
     */
    public function setDebrisFactor($debrisFactor)
    {
        $this->debris_factor = $debrisFactor;

        return $this;
    }

    /**
     * Get debrisFactor
     *
     * @return float
     */
    public function getDebrisFactor()
    {
        return $this->debris_factor;
    }

    /**
     * Set repairFactor
     *
     * @param float $repairFactor
     *
     * @return Univers
     */
    public function setRepairFactor($repairFactor)
    {
        $this->repair_factor = $repairFactor;

        return $this;
    }

    /**
     * Get repairFactor
     *
     * @return float
     */
    public function getRepairFactor()
    {
        return $this->repair_factor;
    }

    /**
     * Set newbieProtectionLimit
     *
     * @param integer $newbieProtectionLimit
     *
     * @return Univers
     */
    public function setNewbieProtectionLimit($newbieProtectionLimit)
    {
        $this->newbie_protection_limit = $newbieProtectionLimit;

        return $this;
    }

    /**
     * Get newbieProtectionLimit
     *
     * @return integer
     */
    public function getNewbieProtectionLimit()
    {
        return $this->newbie_protection_limit;
    }

    /**
     * Set newbieProtectionHigh
     *
     * @param integer $newbieProtectionHigh
     *
     * @return Univers
     */
    public function setNewbieProtectionHigh($newbieProtectionHigh)
    {
        $this->newbie_protection_high = $newbieProtectionHigh;

        return $this;
    }

    /**
     * Get newbieProtectionHigh
     *
     * @return integer
     */
    public function getNewbieProtectionHigh()
    {
        return $this->newbie_protection_high;
    }

    /**
     * Set bonusFields
     *
     * @param integer $bonusFields
     *
     * @return Univers
     */
    public function setBonusFields($bonusFields)
    {
        $this->bonus_fields = $bonusFields;

        return $this;
    }

    /**
     * Get bonusFields
     *
     * @return integer
     */
    public function getBonusFields()
    {
        return $this->bonus_fields;
    }

    /**
     * Set nbGalaxys
     *
     * @param integer $nbGalaxys
     *
     * @return Univers
     */
    public function setNbGalaxys($nbGalaxys)
    {
        $this->nb_galaxys = $nbGalaxys;

        return $this;
    }

    /**
     * Get nbGalaxys
     *
     * @return integer
     */
    public function getNbGalaxys()
    {
        return $this->nb_galaxys;
    }

    /**
     * Set nbSystems
     *
     * @param integer $nbSystems
     *
     * @return Univers
     */
    public function setNbSystems($nbSystems)
    {
        $this->nb_systems = $nbSystems;

        return $this;
    }

    /**
     * Get nbSystems
     *
     * @return integer
     */
    public function getNbSystems()
    {
        return $this->nb_systems;
    }

    /**
     * Set nbPlanetes
     *
     * @param integer $nbPlanetes
     *
     * @return Univers
     */
    public function setNbPlanetes($nbPlanetes)
    {
        $this->nb_planetes = $nbPlanetes;

        return $this;
    }

    /**
     * Get nbPlanetes
     *
     * @return integer
     */
    public function getNbPlanetes()
    {
        return $this->nb_planetes;
    }

    /**
     * Set donutGalaxy
     *
     * @param boolean $donutGalaxy
     *
     * @return Univers
     */
    public function setDonutGalaxy($donutGalaxy)
    {
        $this->donut_galaxy = $donutGalaxy;

        return $this;
    }

    /**
     * Get donutGalaxy
     *
     * @return boolean
     */
    public function getDonutGalaxy()
    {
        return $this->donut_galaxy;
    }

    /**
     * Set donutSystem
     *
     * @param boolean $donutSystem
     *
     * @return Univers
     */
    public function setDonutSystem($donutSystem)
    {
        $this->donut_system = $donutSystem;

        return $this;
    }

    /**
     * Get donutSystem
     *
     * @return boolean
     */
    public function getDonutSystem()
    {
        return $this->donut_system;
    }

    /**
     * Add player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     *
     * @return Univers
     */
    public function addPlayer(\Maesbox\OGInspectorBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     */
    public function removePlayer(\Maesbox\OGInspectorBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add alliance
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Alliance $alliance
     *
     * @return Univers
     */
    public function addAlliance(\Maesbox\OGInspectorBundle\Entity\Alliance $alliance)
    {
        $this->alliances[] = $alliance;

        return $this;
    }

    /**
     * Remove alliance
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Alliance $alliance
     */
    public function removeAlliance(\Maesbox\OGInspectorBundle\Entity\Alliance $alliance)
    {
        $this->alliances->removeElement($alliance);
    }

    /**
     * Get alliances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlliances()
    {
        return $this->alliances;
    }

    /**
     * Add planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     *
     * @return Univers
     */
    public function addPlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete)
    {
        $this->planetes[] = $planete;

        return $this;
    }

    /**
     * Remove planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     */
    public function removePlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete)
    {
        $this->planetes->removeElement($planete);
    }

    /**
     * Get planetes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanetes()
    {
        return $this->planetes;
    }
}
