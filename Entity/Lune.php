<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Lune
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\LuneRepository")
 */
class Lune
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Planete", mappedBy="lune")
     **/
    protected $planete;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;
    
    /**
     * @var integer
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    protected $size;
    
    /**
     * @var integer
     * @ORM\Column(name="fields", type="integer", nullable=true)
     */
    protected $fields;
    
    /**
     * @var integer
     * @ORM\Column(name="min_temp", type="integer", nullable=true)
     */
    protected $min_temp;
    
    /**
     * @var integer
     * @ORM\Column(name="max_temp", type="integer", nullable=true)
     */
    protected $max_temp;
    
    /**
     * @var integer
     * @ORM\Column(name="metal", type="integer", nullable=true)
     */
    protected $metal;
    
    /**
     * @var integer
     * @ORM\Column(name="cristal", type="integer", nullable=true)
     */
    protected $cristal;
    
    /**
     * @var integer
     * @ORM\Column(name="deuterium", type="integer", nullable=true)
     */
    protected $deuterium;
    
    /**
     * @var integer
     * @ORM\Column(name="metal_storage", type="integer", nullable=true)
     */
    protected $metal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="cristal_storage", type="integer", nullable=true)
     */
    protected $cristal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="deuterium_storage", type="integer", nullable=true)
     */
    protected $deuterium_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="secured_metal_storage", type="integer", nullable=true)
     */
    protected $secured_metal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="secured_cristal_storage", type="integer", nullable=true)
     */
    protected $secured_cristal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="secured_deuterium_storage", type="integer", nullable=true)
     */
    protected $secured_deuterium_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="robot_factory", type="integer", nullable=true)
     */
    protected $robot_factory;
    
    /**
     * @var integer
     * @ORM\Column(name="space_factory", type="integer", nullable=true)
     */
    protected $space_factory;
    
    /**
     * @var integer
     * @ORM\Column(name="lunar_base", type="integer", nullable=true)
     */
    protected $lunar_base;
    
    /**
     * @var integer
     * @ORM\Column(name="captors_phalange", type="integer", nullable=true)
     */
    protected $captors_phalange;
    
    /**
     * @var integer
     * @ORM\Column(name="stargate", type="integer", nullable=true)
     */
    protected $stargate;
    
    /**
     * DEFENSES
     */
    
    /**
     * @var integer
     * @ORM\Column(name="missile_launcher", type="integer", nullable=true)
     */
    protected $missile_launcher;
    
    /**
     * @var integer
     * @ORM\Column(name="light_laser", type="integer", nullable=true)
     */
    protected $light_laser;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_laser", type="integer", nullable=true)
     */
    protected $heavy_laser;
    
    /**
     * @var integer
     * @ORM\Column(name="ion_artillery", type="integer", nullable=true)
     */
    protected $ion_artillery;
    
    /**
     * @var integer
     * @ORM\Column(name="gauss_canon", type="integer", nullable=true)
     */
    protected $gauss_canon;
    
    /**
     * @var integer
     * @ORM\Column(name="plasma_launcher", type="integer", nullable=true)
     */
    protected $plasma_launcher;
    
    /**
     * @var integer
     * @ORM\Column(name="satellites", type="integer", nullable=true)
     */
    protected $satellites;
    
    /**
     * @var integer
     * @ORM\Column(name="light_shield", type="boolean", nullable=true)
     */
    protected $light_shield;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_shield", type="boolean", nullable=true)
     */
    protected $heavy_shield;
    
    /**
     * FLOTTE
     */
    
    /**
     * @var integer
     * @ORM\Column(name="light_fighter", type="integer", nullable=true)
     */
    protected $light_fighter;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_fighter", type="integer", nullable=true)
     */
    protected $heavy_fighter;
    
    /**
     * @var integer
     * @ORM\Column(name="cruiser", type="integer", nullable=true)
     */
    protected $cruiser;
    
    /**
     * @var integer
     * @ORM\Column(name="battleship", type="integer", nullable=true)
     */
    protected $battleship;
    
    /**
     * @var integer
     * @ORM\Column(name="tracker", type="integer", nullable=true)
     */
    protected $tracker;
    
    /**
     * @var integer
     * @ORM\Column(name="bomber", type="integer", nullable=true)
     */
    protected $bomber;
    
    /**
     * @var integer
     * @ORM\Column(name="destroyer", type="integer", nullable=true)
     */
    protected $destroyer;
    
    /**
     * @var integer
     * @ORM\Column(name="death_star", type="integer", nullable=true)
     */
    protected $death_star;
    
    /**
     * @var integer
     * @ORM\Column(name="recycler", type="integer", nullable=true)
     */
    protected $recycler;
    
    /**
     * @var integer
     * @ORM\Column(name="light_transporter", type="integer", nullable=true)
     */
    protected $light_transporter;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_transporter", type="integer", nullable=true)
     */
    protected $heavy_transporter;
    
    /**
     * @var integer
     * @ORM\Column(name="spy_probe", type="integer", nullable=true)
     */
    protected $spy_probe;
    
    /**
     * @var integer
     * @ORM\Column(name="colonisation_ship", type="integer", nullable=true)
     */
    protected $colonisation_ship;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     */
    protected $last_updated;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="last_scanned", type="datetime", nullable=true)
     */
    protected $last_scanned;
    
    /**
     * @var OGPlayer
     * @ORM\ManyToOne(targetEntity="Maesbox\OGInspectorBundle\Entity\OGPlayer")
     */
    protected $last_scanned_by;

    /**
     * @ORM\OneToMany(targetEntity="SpyReport", mappedBy="lune")
     **/
    protected $spy_reports;
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->created_at = new \DateTime();
    }
    
    /**
     * @ORM\PostUpdate
     */
    public function setLastUpdateValue()
    {
        $this->last_updated = new \DateTime();
    }
    
    public function getCoord()
    {
        return $this->getPlanete()->getCoord();
    }
    
    public function getMetalStorageCapability()
    {
        return $this->getStorageCapability($this->metal_storage);
    }
    
    public function getCristalStorageCapability()
    {
        return $this->getStorageCapability($this->cristal_storage);
    }

    public function getDeuteriumStorageCapability()
    {
        return $this->getStorageCapability($this->deuterium_storage);
    }

    private function getStorageCapability($lvl)
    {
        if( $lvl === 0 || $lvl === null )
        {
            $capability =  10000 ;
        }
        else
        {
            $capability = round( pow(2.5, (20 * $lvl/ 33)),0) * 20000;
        }
        
        return $capability;
    }

    public function getBuildingsPoints()
    {
        return intval(($this->getBuildingsValue()/1000));
    }

    public function getFleetPoints()
    {
        return intval(($this->getFleetValue()/1000));
    }
    
    public function getDefensePoints()
    {
        return intval(($this->getDefenseValue()/1000));
    }
    
    /**
     * Get fleet value
     * 
     * @return integer
     */
    public function getFleetValue()
    {
        return  $this->light_fighter * 4000 +
                $this->heavy_fighter * 10000 +
                $this->cruiser * 29000 +
                $this->tracker * 85000 +
                $this->battleship * 60000 +
                $this->bomber * 90000 +
                $this->destroyer * 125000 +
                $this->death_star * 10000000 +
                $this->light_transporter * 4000 +
                $this->heavy_transporter * 12000 +
                $this->recycler * 18000 +
                $this->colonisation_ship * 40000 +
                $this->spy_probe * 1000 +
                $this->satellites * 2500;
    }
    
    public function getMilitaryFleetValue()
    {
        return  $this->light_fighter * 4000 +
                $this->heavy_fighter * 10000 +
                $this->cruiser * 29000 +
                $this->tracker * 85000 +
                $this->battleship * 60000 +
                $this->bomber * 90000 +
                $this->destroyer * 125000 +
                $this->death_star * 10000000 +
                $this->light_transporter * 4000 +
                $this->heavy_transporter * 12000 +
                $this->recycler * 18000 +
                $this->colonisation_ship * 40000 +
                $this->spy_probe * 1000 ;
    }
    
    /**
     * Get defense value
     * 
     * @return integer
     */
    public function getDefenseValue()
    {
        return  $this->missile_launcher * 2000 +
                $this->light_laser * 2000 +
                $this->heavy_laser * 8000 +
                $this->ion_artillery * 8000 +
                $this->gauss_canon * 37000 +
                $this->plasma_launcher * 130000 +
                $this->light_shield * 20000 +
                $this->heavy_shield * 100000;
    }
    
    public function getNbFieldsUsed()
    {
        return  $this->metal_storage+
                $this->cristal_storage +
                $this->deuterium_storage +
                
                $this->secured_metal_storage +
                $this->secured_cristal_storage +
                $this->secured_deuterium_storage +
                
                $this->robot_factory +
                $this->space_factory +
                $this->stargate +
                $this->lunar_base +
                $this->captors_phalange;
    }
    
    
    /**
     * Get value for all buildings
     * 
     * @return integer
     */
    public function getBuildingsValue()
    {
        return  $this->getBuildingValue($this->metal_storage, 2, array(1000,0,0)) +
                $this->getBuildingValue($this->cristal_storage, 2, array(1000,500,0)) +
                $this->getBuildingValue($this->deuterium_storage, 2, array(1000,1000,0)) +
                
                $this->getBuildingValue($this->secured_metal_storage, 2.3, array(2645,0,0)) +
                $this->getBuildingValue($this->secured_cristal_storage, 2.3, array(2645,1322,0)) +
                $this->getBuildingValue($this->secured_deuterium_storage, 2.3, array(2645,2645,0)) +
                
                $this->getBuildingValue($this->space_factory, 2, array(400,200,100)) +
                $this->getBuildingValue($this->robot_factory, 2, array(400,120,200)) +
                $this->getBuildingValue($this->stargate, 2, array(2000000,4000000,2000000)) +
                $this->getBuildingValue($this->lunar_base, 2, array(20000,40000,20000)) +
                $this->getBuildingValue($this->captors_phalange, 2, array(20000,40000,20000));
    }

    private function getBuildingValue( $lvl, $coef, array $base_price )
    {
        $value = 0;
        
        for($i =1; $i <= $lvl; $i++)
        {
            $value += $this->getBuildingLvlValue($i, $coef, $base_price);
        }
        
        return round($value, 0);
    }
    
    private function getBuildingLvlValue($lvl, $coef, array $base_price)
    {
        $metal_value = $base_price[0] * pow($coef ,$lvl - 1 );
        $cristal_value = $base_price[1] * pow($coef ,$lvl - 1 );
        $deut_value = $base_price[2] * pow($coef ,$lvl - 1 );
        
        return ($metal_value+$cristal_value+$deut_value);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     * @return Lune
     */
    public function setPlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete = null)
    {
        $this->planete = $planete;
    
        return $this;
    }

    /**
     * Get planete
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Planete 
     */
    public function getPlanete()
    {
        return $this->planete;
    }

    /**
     * Set last_updated
     *
     * @param \DateTime $lastUpdated
     * @return Lune
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->last_updated = $lastUpdated;
    
        return $this;
    }

    /**
     * Get last_updated
     *
     * @return \DateTime 
     */
    public function getLastUpdated()
    {
        return $this->last_updated;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Lune
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set last_scanned
     *
     * @param \DateTime $lastScanned
     * @return Lune
     */
    public function setLastScanned($lastScanned)
    {
        $this->last_scanned = $lastScanned;
    
        return $this;
    }

    /**
     * Get last_scanned
     *
     * @return \DateTime 
     */
    public function getLastScanned()
    {
        return $this->last_scanned;
    }

    /**
     * Set last_scanned_by
     *
     * @param OGPlayer $lastScannedBy
     * @return Lune
     */
    public function setLastScannedBy(OGPlayer $lastScannedBy = null)
    {
        $this->last_scanned_by = $lastScannedBy;
    
        return $this;
    }

    /**
     * Get last_scanned_by
     *
     * @return OGPlayer
     */
    public function getLastScannedBy()
    {
        return $this->last_scanned_by;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Lune
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set metal
     *
     * @param integer $metal
     * @return Lune
     */
    public function setMetal($metal)
    {
        $this->metal = $metal;
    
        return $this;
    }

    /**
     * Get metal
     *
     * @return integer 
     */
    public function getMetal()
    {
        return $this->metal;
    }

    /**
     * Set cristal
     *
     * @param integer $cristal
     * @return Lune
     */
    public function setCristal($cristal)
    {
        $this->cristal = $cristal;
    
        return $this;
    }

    /**
     * Get cristal
     *
     * @return integer 
     */
    public function getCristal()
    {
        return $this->cristal;
    }

    /**
     * Set deuterium
     *
     * @param integer $deuterium
     * @return Lune
     */
    public function setDeuterium($deuterium)
    {
        $this->deuterium = $deuterium;
    
        return $this;
    }

    /**
     * Get deuterium
     *
     * @return integer 
     */
    public function getDeuterium()
    {
        return $this->deuterium;
    }

    /**
     * Set metal_storage
     *
     * @param integer $metalStorage
     * @return Lune
     */
    public function setMetalStorage($metalStorage)
    {
        $this->metal_storage = $metalStorage;
    
        return $this;
    }

    /**
     * Get metal_storage
     *
     * @return integer 
     */
    public function getMetalStorage()
    {
        return $this->metal_storage;
    }

    /**
     * Set cristal_storage
     *
     * @param integer $cristalStorage
     * @return Lune
     */
    public function setCristalStorage($cristalStorage)
    {
        $this->cristal_storage = $cristalStorage;
    
        return $this;
    }

    /**
     * Get cristal_storage
     *
     * @return integer 
     */
    public function getCristalStorage()
    {
        return $this->cristal_storage;
    }

    /**
     * Set deuterium_storage
     *
     * @param integer $deuteriumStorage
     * @return Lune
     */
    public function setDeuteriumStorage($deuteriumStorage)
    {
        $this->deuterium_storage = $deuteriumStorage;
    
        return $this;
    }

    /**
     * Get deuterium_storage
     *
     * @return integer 
     */
    public function getDeuteriumStorage()
    {
        return $this->deuterium_storage;
    }

    /**
     * Set secured_metal_storage
     *
     * @param integer $securedMetalStorage
     * @return Lune
     */
    public function setSecuredMetalStorage($securedMetalStorage)
    {
        $this->secured_metal_storage = $securedMetalStorage;
    
        return $this;
    }

    /**
     * Get secured_metal_storage
     *
     * @return integer 
     */
    public function getSecuredMetalStorage()
    {
        return $this->secured_metal_storage;
    }

    /**
     * Set secured_cristal_storage
     *
     * @param integer $securedCristalStorage
     * @return Lune
     */
    public function setSecuredCristalStorage($securedCristalStorage)
    {
        $this->secured_cristal_storage = $securedCristalStorage;
    
        return $this;
    }

    /**
     * Get secured_cristal_storage
     *
     * @return integer 
     */
    public function getSecuredCristalStorage()
    {
        return $this->secured_cristal_storage;
    }

    /**
     * Set secured_deuterium_storage
     *
     * @param integer $securedDeuteriumStorage
     * @return Lune
     */
    public function setSecuredDeuteriumStorage($securedDeuteriumStorage)
    {
        $this->secured_deuterium_storage = $securedDeuteriumStorage;
    
        return $this;
    }

    /**
     * Get secured_deuterium_storage
     *
     * @return integer 
     */
    public function getSecuredDeuteriumStorage()
    {
        return $this->secured_deuterium_storage;
    }

    /**
     * Set robot_factory
     *
     * @param integer $robotFactory
     * @return Lune
     */
    public function setRobotFactory($robotFactory)
    {
        $this->robot_factory = $robotFactory;
    
        return $this;
    }

    /**
     * Get robot_factory
     *
     * @return integer 
     */
    public function getRobotFactory()
    {
        return $this->robot_factory;
    }
    /**
     * Set space_factory
     *
     * @param integer $spaceFactory
     * @return Lune
     */
    public function setSpaceFactory($spaceFactory)
    {
        $this->space_factory = $spaceFactory;
    
        return $this;
    }

    /**
     * Get space_factory
     *
     * @return integer 
     */
    public function getSpaceFactory()
    {
        return $this->space_factory;
    }

    /**
     * Set lunar_base
     *
     * @param integer $lunarBase
     * @return Lune
     */
    public function setLunarBase($lunarBase)
    {
        $this->lunar_base = $lunarBase;
    
        return $this;
    }

    /**
     * Get lunar_base
     *
     * @return integer 
     */
    public function getLunarBase()
    {
        return $this->lunar_base;
    }

    /**
     * Set captors_phalange
     *
     * @param integer $captorsPhalange
     * @return Lune
     */
    public function setCaptorsPhalange($captorsPhalange)
    {
        $this->captors_phalange = $captorsPhalange;
    
        return $this;
    }

    /**
     * Get captors_phalange
     *
     * @return integer 
     */
    public function getCaptorsPhalange()
    {
        return $this->captors_phalange;
    }

    /**
     * Set stargate
     *
     * @param integer $stargate
     * @return Lune
     */
    public function setStargate($stargate)
    {
        $this->stargate = $stargate;
    
        return $this;
    }

    /**
     * Get stargate
     *
     * @return integer 
     */
    public function getStargate()
    {
        return $this->stargate;
    }

    /**
     * Set missile_launcher
     *
     * @param integer $missileLauncher
     * @return Lune
     */
    public function setMissileLauncher($missileLauncher)
    {
        $this->missile_launcher = $missileLauncher;
    
        return $this;
    }

    /**
     * Get missile_launcher
     *
     * @return integer 
     */
    public function getMissileLauncher()
    {
        return $this->missile_launcher;
    }

    /**
     * Set light_laser
     *
     * @param integer $lightLaser
     * @return Lune
     */
    public function setLightLaser($lightLaser)
    {
        $this->light_laser = $lightLaser;
    
        return $this;
    }

    /**
     * Get light_laser
     *
     * @return integer 
     */
    public function getLightLaser()
    {
        return $this->light_laser;
    }

    /**
     * Set heavy_laser
     *
     * @param integer $heavyLaser
     * @return Lune
     */
    public function setHeavyLaser($heavyLaser)
    {
        $this->heavy_laser = $heavyLaser;
    
        return $this;
    }

    /**
     * Get heavy_laser
     *
     * @return integer 
     */
    public function getHeavyLaser()
    {
        return $this->heavy_laser;
    }

    /**
     * Set ion_artillery
     *
     * @param integer $ionArtillery
     * @return Lune
     */
    public function setIonArtillery($ionArtillery)
    {
        $this->ion_artillery = $ionArtillery;
    
        return $this;
    }

    /**
     * Get ion_artillery
     *
     * @return integer 
     */
    public function getIonArtillery()
    {
        return $this->ion_artillery;
    }

    /**
     * Set gauss_canon
     *
     * @param integer $gaussCanon
     * @return Lune
     */
    public function setGaussCanon($gaussCanon)
    {
        $this->gauss_canon = $gaussCanon;
    
        return $this;
    }

    /**
     * Get gauss_canon
     *
     * @return integer 
     */
    public function getGaussCanon()
    {
        return $this->gauss_canon;
    }

    /**
     * Set plasma_launcher
     *
     * @param integer $plasmaLauncher
     * @return Lune
     */
    public function setPlasmaLauncher($plasmaLauncher)
    {
        $this->plasma_launcher = $plasmaLauncher;
    
        return $this;
    }

    /**
     * Get plasma_launcher
     *
     * @return integer 
     */
    public function getPlasmaLauncher()
    {
        return $this->plasma_launcher;
    }

    /**
     * Set satellites
     *
     * @param integer $satellites
     * @return Lune
     */
    public function setSatellites($satellites)
    {
        $this->satellites = $satellites;
    
        return $this;
    }

    /**
     * Get satellites
     *
     * @return integer 
     */
    public function getSatellites()
    {
        return $this->satellites;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Lune
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set light_shield
     *
     * @param boolean $lightShield
     * @return Lune
     */
    public function setLightShield($lightShield)
    {
        $this->light_shield = $lightShield;
    
        return $this;
    }

    /**
     * Get light_shield
     *
     * @return boolean 
     */
    public function getLightShield()
    {
        return $this->light_shield;
    }

    /**
     * Set heavy_shield
     *
     * @param boolean $heavyShield
     * @return Lune
     */
    public function setHeavyShield($heavyShield)
    {
        $this->heavy_shield = $heavyShield;
    
        return $this;
    }

    /**
     * Get heavy_shield
     *
     * @return boolean 
     */
    public function getHeavyShield()
    {
        return $this->heavy_shield;
    }

    /**
     * Set fields
     *
     * @param integer $fields
     * @return Lune
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    
        return $this;
    }

    /**
     * Get fields
     *
     * @return integer 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set min_temp
     *
     * @param integer $minTemp
     * @return Lune
     */
    public function setMinTemp($minTemp)
    {
        $this->min_temp = $minTemp;
    
        return $this;
    }

    /**
     * Get min_temp
     *
     * @return integer 
     */
    public function getMinTemp()
    {
        return $this->min_temp;
    }

    /**
     * Set max_temp
     *
     * @param integer $maxTemp
     * @return Lune
     */
    public function setMaxTemp($maxTemp)
    {
        $this->max_temp = $maxTemp;
    
        return $this;
    }

    /**
     * Get max_temp
     *
     * @return integer 
     */
    public function getMaxTemp()
    {
        return $this->max_temp;
    }

    /**
     * Set light_fighter
     *
     * @param integer $lightFighter
     * @return Lune
     */
    public function setLightFighter($lightFighter)
    {
        $this->light_fighter = $lightFighter;
    
        return $this;
    }

    /**
     * Get light_fighter
     *
     * @return integer 
     */
    public function getLightFighter()
    {
        return $this->light_fighter;
    }

    /**
     * Set heavy_fighter
     *
     * @param integer $heavyFighter
     * @return Lune
     */
    public function setHeavyFighter($heavyFighter)
    {
        $this->heavy_fighter = $heavyFighter;
    
        return $this;
    }

    /**
     * Get heavy_fighter
     *
     * @return integer 
     */
    public function getHeavyFighter()
    {
        return $this->heavy_fighter;
    }

    /**
     * Set cruiser
     *
     * @param integer $cruiser
     * @return Lune
     */
    public function setCruiser($cruiser)
    {
        $this->cruiser = $cruiser;
    
        return $this;
    }

    /**
     * Get cruiser
     *
     * @return integer 
     */
    public function getCruiser()
    {
        return $this->cruiser;
    }

    /**
     * Set battleship
     *
     * @param integer $battleship
     * @return Lune
     */
    public function setBattleship($battleship)
    {
        $this->battleship = $battleship;
    
        return $this;
    }

    /**
     * Get battleship
     *
     * @return integer 
     */
    public function getBattleship()
    {
        return $this->battleship;
    }

    /**
     * Set tracker
     *
     * @param integer $tracker
     * @return Lune
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;
    
        return $this;
    }

    /**
     * Get tracker
     *
     * @return integer 
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set bomber
     *
     * @param integer $bomber
     * @return Lune
     */
    public function setBomber($bomber)
    {
        $this->bomber = $bomber;
    
        return $this;
    }

    /**
     * Get bomber
     *
     * @return integer 
     */
    public function getBomber()
    {
        return $this->bomber;
    }

    /**
     * Set destroyer
     *
     * @param integer $destroyer
     * @return Lune
     */
    public function setDestroyer($destroyer)
    {
        $this->destroyer = $destroyer;
    
        return $this;
    }

    /**
     * Get destroyer
     *
     * @return integer 
     */
    public function getDestroyer()
    {
        return $this->destroyer;
    }

    /**
     * Set death_star
     *
     * @param integer $deathStar
     * @return Lune
     */
    public function setDeathStar($deathStar)
    {
        $this->death_star = $deathStar;
    
        return $this;
    }

    /**
     * Get death_star
     *
     * @return integer 
     */
    public function getDeathStar()
    {
        return $this->death_star;
    }

    /**
     * Set recycler
     *
     * @param integer $recycler
     * @return Lune
     */
    public function setRecycler($recycler)
    {
        $this->recycler = $recycler;
    
        return $this;
    }

    /**
     * Get recycler
     *
     * @return integer 
     */
    public function getRecycler()
    {
        return $this->recycler;
    }

    /**
     * Set light_transporter
     *
     * @param integer $lightTransporter
     * @return Lune
     */
    public function setLightTransporter($lightTransporter)
    {
        $this->light_transporter = $lightTransporter;
    
        return $this;
    }

    /**
     * Get light_transporter
     *
     * @return integer 
     */
    public function getLightTransporter()
    {
        return $this->light_transporter;
    }

    /**
     * Set heavy_transporter
     *
     * @param integer $heavyTransporter
     * @return Lune
     */
    public function setHeavyTransporter($heavyTransporter)
    {
        $this->heavy_transporter = $heavyTransporter;
    
        return $this;
    }

    /**
     * Get heavy_transporter
     *
     * @return integer 
     */
    public function getHeavyTransporter()
    {
        return $this->heavy_transporter;
    }

    /**
     * Set spy_probe
     *
     * @param integer $spyProbe
     * @return Lune
     */
    public function setSpyProbe($spyProbe)
    {
        $this->spy_probe = $spyProbe;
    
        return $this;
    }

    /**
     * Get spy_probe
     *
     * @return integer 
     */
    public function getSpyProbe()
    {
        return $this->spy_probe;
    }

    /**
     * Set colonisation_ship
     *
     * @param integer $colonisationShip
     * @return Lune
     */
    public function setColonisationShip($colonisationShip)
    {
        $this->colonisation_ship = $colonisationShip;
    
        return $this;
    }

    /**
     * Get colonisation_ship
     *
     * @return integer 
     */
    public function getColonisationShip()
    {
        return $this->colonisation_ship;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spy_reports = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add spy_reports
     *
     * @param \Maesbox\OGInspectorBundle\Entity\SpyReport $spyReports
     * @return Lune
     */
    public function addSpyReport(\Maesbox\OGInspectorBundle\Entity\SpyReport $spyReports)
    {
        $this->spy_reports[] = $spyReports;
    
        return $this;
    }

    /**
     * Remove spy_reports
     *
     * @param \Maesbox\OGInspectorBundle\Entity\SpyReport $spyReports
     */
    public function removeSpyReport(\Maesbox\OGInspectorBundle\Entity\SpyReport $spyReports)
    {
        $this->spy_reports->removeElement($spyReports);
    }

    /**
     * Get spy_reports
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpyReports()
    {
        return $this->spy_reports;
    }
}
