<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * SpyReport
 * 
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class SpyReport
{        
    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="Maesbox\OGInspectorBundle\Entity\Player",inversedBy="spy_reports" )
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", nullable=true)
     */
    protected $player;
    
    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="Maesbox\OGInspectorBundle\Entity\Player" )
     * @ORM\JoinColumn(name="target_player_id", referencedColumnName="id", nullable=true)
     */
    protected $target;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;
    
    /**
     * @var integer
     * @ORM\Column(name="metal", type="integer", nullable=true)
     */
    protected $metal;
    
    /**
     * @var integer
     * @ORM\Column(name="cristal", type="integer", nullable=true)
     */
    protected $cristal;
    
    /**
     * @var integer
     * @ORM\Column(name="deuterium", type="integer", nullable=true)
     */
    protected $deuterium;
    
    
    /**
     * @var integer
     * @ORM\Column(name="robot_factory", type="integer", nullable=true)
     */
    protected $robot_factory;
    
    /**
     * @var integer
     * @ORM\Column(name="space_factory", type="integer", nullable=true)
     */
    protected $space_factory;
    
    /**
     * RESEARCH
     */
    
    /**
     * @var integer
     * @ORM\Column(name="energy_technology", type="integer", nullable=true)
     */
    protected $energy_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="laser_technology", type="integer", nullable=true)
     */
    protected $laser_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="ion_technology", type="integer", nullable=true)
     */
    protected $ion_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="plasma_technology", type="integer", nullable=true)
     */
    protected $plasma_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="hyperspace_technology", type="integer", nullable=true)
     */
    protected $hyperspace_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="combustion_propulsion_technology", type="integer", nullable=true)
     */
    protected $combustion_propulsion_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="impulsion_propulsion_technology", type="integer", nullable=true)
     */
    protected $impulsion_propulsion_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="hyperspace_propulsion_technology", type="integer", nullable=true)
     */
    protected $hyperspace_propulsion_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="spy_technology", type="integer", nullable=true)
     */
    protected $spy_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="computer_technology", type="integer", nullable=true)
     */
    protected $computer_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="astrophysic_technology", type="integer", nullable=true)
     */
    protected $astrophysic_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="research_network_technology", type="integer", nullable=true)
     */
    protected $research_network_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="graviton_technology", type="integer", nullable=true)
     */
    protected $graviton_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="weapon_technology", type="integer", nullable=true)
     */
    protected $weapon_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="shield_technology", type="integer", nullable=true)
     */
    protected $shield_technology;
    
    /**
     * @var integer
     * @ORM\Column(name="structure_technology", type="integer", nullable=true)
     */
    protected $structure_technology;
    
    
    /**
     * DEFENSES
     */
    
    /**
     * @var integer
     * @ORM\Column(name="missile_launcher", type="integer", nullable=true)
     */
    protected $missile_launcher;
    
    /**
     * @var integer
     * @ORM\Column(name="light_laser", type="integer", nullable=true)
     */
    protected $light_laser;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_laser", type="integer", nullable=true)
     */
    protected $heavy_laser;
    
    /**
     * @var integer
     * @ORM\Column(name="ion_artillery", type="integer", nullable=true)
     */
    protected $ion_artillery;
    
    /**
     * @var integer
     * @ORM\Column(name="gauss_canon", type="integer", nullable=true)
     */
    protected $gauss_canon;
    
    /**
     * @var integer
     * @ORM\Column(name="plasma_launcher", type="integer", nullable=true)
     */
    protected $plasma_launcher;
    
    /**
     * @var integer
     * @ORM\Column(name="satellites", type="integer", nullable=true)
     */
    protected $satellites;
    
    /**
     * @var integer
     * @ORM\Column(name="light_shield", type="boolean", nullable=true)
     */
    protected $light_shield;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_shield", type="boolean", nullable=true)
     */
    protected $heavy_shield;
    
    /**
     * FLOTTE
     */
    
    /**
     * @var integer
     * @ORM\Column(name="light_fighter", type="integer", nullable=true)
     */
    protected $light_fighter;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_fighter", type="integer", nullable=true)
     */
    protected $heavy_fighter;
    
    /**
     * @var integer
     * @ORM\Column(name="cruiser", type="integer", nullable=true)
     */
    protected $cruiser;
    
    /**
     * @var integer
     * @ORM\Column(name="battleship", type="integer", nullable=true)
     */
    protected $battleship;
    
    /**
     * @var integer
     * @ORM\Column(name="tracker", type="integer", nullable=true)
     */
    protected $tracker;
    
    /**
     * @var integer
     * @ORM\Column(name="bomber", type="integer", nullable=true)
     */
    protected $bomber;
    
    /**
     * @var integer
     * @ORM\Column(name="destroyer", type="integer", nullable=true)
     */
    protected $destroyer;
    
    /**
     * @var integer
     * @ORM\Column(name="death_star", type="integer", nullable=true)
     */
    protected $death_star;
    
    /**
     * @var integer
     * @ORM\Column(name="recycler", type="integer", nullable=true)
     */
    protected $recycler;
    
    /**
     * @var integer
     * @ORM\Column(name="light_transporter", type="integer", nullable=true)
     */
    protected $light_transporter;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_transporter", type="integer", nullable=true)
     */
    protected $heavy_transporter;
    
    /**
     * @var integer
     * @ORM\Column(name="spy_probe", type="integer", nullable=true)
     */
    protected $spy_probe;
    
    /**
     * @var integer
     * @ORM\Column(name="colonisation_ship", type="integer", nullable=true)
     */
    protected $colonisation_ship;

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtValue(){
        $this->setCreatedAt(new \DateTime());
    }
}
