<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * SpyReport
 * 
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\PlaneteSpyReportRepository")
 * 
 */
class PlaneteSpyReport extends SpyReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Maesbox\OGInspectorBundle\Entity\Planete",inversedBy="spy_reports" )
     * @ORM\JoinColumn(name="planete_id", referencedColumnName="id", nullable=true)
     **/
    protected $planete;
    
    /**
     * PLANETE BATIMENTS
     */
    
    /**
     * @var integer
     * @ORM\Column(name="metal_mine", type="integer", nullable=true)
     */
    protected $metal_mine;
    
    /**
     * @var integer
     * @ORM\Column(name="cristal_mine", type="integer", nullable=true)
     */
    protected $cristal_mine;
    
    /**
     * @var integer
     * @ORM\Column(name="deuterium_mine", type="integer", nullable=true)
     */
    protected $deuterium_mine;
    
    /**
     * @var integer
     * @ORM\Column(name="metal_storage", type="integer", nullable=true)
     */
    protected $metal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="cristal_storage", type="integer", nullable=true)
     */
    protected $cristal_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="deuterium_storage", type="integer", nullable=true)
     */
    protected $deuterium_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="solar_central", type="integer", nullable=true)
     */
    protected $solar_central;
    
    /**
     * @var integer
     * @ORM\Column(name="fusion_central", type="integer", nullable=true)
     */
    protected $fusion_central;
    
    /**
     * PLANETE INSTALLATIONS
     */
    
    /**
     * @var integer
     * @ORM\Column(name="nanite_factory", type="integer", nullable=true)
     */
    protected $nanite_factory;
    
    /**
     * @var integer
     * @ORM\Column(name="research_lab", type="integer", nullable=true)
     */
    protected $research_lab;
    
    /**
     * @var integer
     * @ORM\Column(name="missile_silo", type="integer", nullable=true)
     */
    protected $missile_silo;
    
    /**
     * @var integer
     * @ORM\Column(name="supply_storage", type="integer", nullable=true)
     */
    protected $supply_storage;
    
    /**
     * @var integer
     * @ORM\Column(name="terraformer", type="integer", nullable=true)
     */
    protected $terraformer;

    /**
     * MISSILES
     */
    
    /**
     * @var integer
     * @ORM\Column(name="defense_missile", type="boolean", nullable=true)
     */
    protected $light_shield;
    
    /**
     * @var integer
     * @ORM\Column(name="heavy_shield", type="boolean", nullable=true)
     */
    protected $heavy_shield;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set metalMine
     *
     * @param integer $metalMine
     *
     * @return PlaneteSpyReport
     */
    public function setMetalMine($metalMine)
    {
        $this->metal_mine = $metalMine;

        return $this;
    }

    /**
     * Get metalMine
     *
     * @return integer
     */
    public function getMetalMine()
    {
        return $this->metal_mine;
    }

    /**
     * Set cristalMine
     *
     * @param integer $cristalMine
     *
     * @return PlaneteSpyReport
     */
    public function setCristalMine($cristalMine)
    {
        $this->cristal_mine = $cristalMine;

        return $this;
    }

    /**
     * Get cristalMine
     *
     * @return integer
     */
    public function getCristalMine()
    {
        return $this->cristal_mine;
    }

    /**
     * Set deuteriumMine
     *
     * @param integer $deuteriumMine
     *
     * @return PlaneteSpyReport
     */
    public function setDeuteriumMine($deuteriumMine)
    {
        $this->deuterium_mine = $deuteriumMine;

        return $this;
    }

    /**
     * Get deuteriumMine
     *
     * @return integer
     */
    public function getDeuteriumMine()
    {
        return $this->deuterium_mine;
    }

    /**
     * Set metalStorage
     *
     * @param integer $metalStorage
     *
     * @return PlaneteSpyReport
     */
    public function setMetalStorage($metalStorage)
    {
        $this->metal_storage = $metalStorage;

        return $this;
    }

    /**
     * Get metalStorage
     *
     * @return integer
     */
    public function getMetalStorage()
    {
        return $this->metal_storage;
    }

    /**
     * Set cristalStorage
     *
     * @param integer $cristalStorage
     *
     * @return PlaneteSpyReport
     */
    public function setCristalStorage($cristalStorage)
    {
        $this->cristal_storage = $cristalStorage;

        return $this;
    }

    /**
     * Get cristalStorage
     *
     * @return integer
     */
    public function getCristalStorage()
    {
        return $this->cristal_storage;
    }

    /**
     * Set deuteriumStorage
     *
     * @param integer $deuteriumStorage
     *
     * @return PlaneteSpyReport
     */
    public function setDeuteriumStorage($deuteriumStorage)
    {
        $this->deuterium_storage = $deuteriumStorage;

        return $this;
    }

    /**
     * Get deuteriumStorage
     *
     * @return integer
     */
    public function getDeuteriumStorage()
    {
        return $this->deuterium_storage;
    }

    /**
     * Set solarCentral
     *
     * @param integer $solarCentral
     *
     * @return PlaneteSpyReport
     */
    public function setSolarCentral($solarCentral)
    {
        $this->solar_central = $solarCentral;

        return $this;
    }

    /**
     * Get solarCentral
     *
     * @return integer
     */
    public function getSolarCentral()
    {
        return $this->solar_central;
    }

    /**
     * Set fusionCentral
     *
     * @param integer $fusionCentral
     *
     * @return PlaneteSpyReport
     */
    public function setFusionCentral($fusionCentral)
    {
        $this->fusion_central = $fusionCentral;

        return $this;
    }

    /**
     * Get fusionCentral
     *
     * @return integer
     */
    public function getFusionCentral()
    {
        return $this->fusion_central;
    }

    /**
     * Set naniteFactory
     *
     * @param integer $naniteFactory
     *
     * @return PlaneteSpyReport
     */
    public function setNaniteFactory($naniteFactory)
    {
        $this->nanite_factory = $naniteFactory;

        return $this;
    }

    /**
     * Get naniteFactory
     *
     * @return integer
     */
    public function getNaniteFactory()
    {
        return $this->nanite_factory;
    }

    /**
     * Set researchLab
     *
     * @param integer $researchLab
     *
     * @return PlaneteSpyReport
     */
    public function setResearchLab($researchLab)
    {
        $this->research_lab = $researchLab;

        return $this;
    }

    /**
     * Get researchLab
     *
     * @return integer
     */
    public function getResearchLab()
    {
        return $this->research_lab;
    }

    /**
     * Set missileSilo
     *
     * @param integer $missileSilo
     *
     * @return PlaneteSpyReport
     */
    public function setMissileSilo($missileSilo)
    {
        $this->missile_silo = $missileSilo;

        return $this;
    }

    /**
     * Get missileSilo
     *
     * @return integer
     */
    public function getMissileSilo()
    {
        return $this->missile_silo;
    }

    /**
     * Set supplyStorage
     *
     * @param integer $supplyStorage
     *
     * @return PlaneteSpyReport
     */
    public function setSupplyStorage($supplyStorage)
    {
        $this->supply_storage = $supplyStorage;

        return $this;
    }

    /**
     * Get supplyStorage
     *
     * @return integer
     */
    public function getSupplyStorage()
    {
        return $this->supply_storage;
    }

    /**
     * Set terraformer
     *
     * @param integer $terraformer
     *
     * @return PlaneteSpyReport
     */
    public function setTerraformer($terraformer)
    {
        $this->terraformer = $terraformer;

        return $this;
    }

    /**
     * Get terraformer
     *
     * @return integer
     */
    public function getTerraformer()
    {
        return $this->terraformer;
    }

    /**
     * Set lightShield
     *
     * @param boolean $lightShield
     *
     * @return PlaneteSpyReport
     */
    public function setLightShield($lightShield)
    {
        $this->light_shield = $lightShield;

        return $this;
    }

    /**
     * Get lightShield
     *
     * @return boolean
     */
    public function getLightShield()
    {
        return $this->light_shield;
    }

    /**
     * Set heavyShield
     *
     * @param boolean $heavyShield
     *
     * @return PlaneteSpyReport
     */
    public function setHeavyShield($heavyShield)
    {
        $this->heavy_shield = $heavyShield;

        return $this;
    }

    /**
     * Get heavyShield
     *
     * @return boolean
     */
    public function getHeavyShield()
    {
        return $this->heavy_shield;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PlaneteSpyReport
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set metal
     *
     * @param integer $metal
     *
     * @return PlaneteSpyReport
     */
    public function setMetal($metal)
    {
        $this->metal = $metal;

        return $this;
    }

    /**
     * Get metal
     *
     * @return integer
     */
    public function getMetal()
    {
        return $this->metal;
    }

    /**
     * Set cristal
     *
     * @param integer $cristal
     *
     * @return PlaneteSpyReport
     */
    public function setCristal($cristal)
    {
        $this->cristal = $cristal;

        return $this;
    }

    /**
     * Get cristal
     *
     * @return integer
     */
    public function getCristal()
    {
        return $this->cristal;
    }

    /**
     * Set deuterium
     *
     * @param integer $deuterium
     *
     * @return PlaneteSpyReport
     */
    public function setDeuterium($deuterium)
    {
        $this->deuterium = $deuterium;

        return $this;
    }

    /**
     * Get deuterium
     *
     * @return integer
     */
    public function getDeuterium()
    {
        return $this->deuterium;
    }

    /**
     * Set robotFactory
     *
     * @param integer $robotFactory
     *
     * @return PlaneteSpyReport
     */
    public function setRobotFactory($robotFactory)
    {
        $this->robot_factory = $robotFactory;

        return $this;
    }

    /**
     * Get robotFactory
     *
     * @return integer
     */
    public function getRobotFactory()
    {
        return $this->robot_factory;
    }

    /**
     * Set spaceFactory
     *
     * @param integer $spaceFactory
     *
     * @return PlaneteSpyReport
     */
    public function setSpaceFactory($spaceFactory)
    {
        $this->space_factory = $spaceFactory;

        return $this;
    }

    /**
     * Get spaceFactory
     *
     * @return integer
     */
    public function getSpaceFactory()
    {
        return $this->space_factory;
    }

    /**
     * Set energyTechnology
     *
     * @param integer $energyTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setEnergyTechnology($energyTechnology)
    {
        $this->energy_technology = $energyTechnology;

        return $this;
    }

    /**
     * Get energyTechnology
     *
     * @return integer
     */
    public function getEnergyTechnology()
    {
        return $this->energy_technology;
    }

    /**
     * Set laserTechnology
     *
     * @param integer $laserTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setLaserTechnology($laserTechnology)
    {
        $this->laser_technology = $laserTechnology;

        return $this;
    }

    /**
     * Get laserTechnology
     *
     * @return integer
     */
    public function getLaserTechnology()
    {
        return $this->laser_technology;
    }

    /**
     * Set ionTechnology
     *
     * @param integer $ionTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setIonTechnology($ionTechnology)
    {
        $this->ion_technology = $ionTechnology;

        return $this;
    }

    /**
     * Get ionTechnology
     *
     * @return integer
     */
    public function getIonTechnology()
    {
        return $this->ion_technology;
    }

    /**
     * Set plasmaTechnology
     *
     * @param integer $plasmaTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setPlasmaTechnology($plasmaTechnology)
    {
        $this->plasma_technology = $plasmaTechnology;

        return $this;
    }

    /**
     * Get plasmaTechnology
     *
     * @return integer
     */
    public function getPlasmaTechnology()
    {
        return $this->plasma_technology;
    }

    /**
     * Set hyperspaceTechnology
     *
     * @param integer $hyperspaceTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setHyperspaceTechnology($hyperspaceTechnology)
    {
        $this->hyperspace_technology = $hyperspaceTechnology;

        return $this;
    }

    /**
     * Get hyperspaceTechnology
     *
     * @return integer
     */
    public function getHyperspaceTechnology()
    {
        return $this->hyperspace_technology;
    }

    /**
     * Set combustionPropulsionTechnology
     *
     * @param integer $combustionPropulsionTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setCombustionPropulsionTechnology($combustionPropulsionTechnology)
    {
        $this->combustion_propulsion_technology = $combustionPropulsionTechnology;

        return $this;
    }

    /**
     * Get combustionPropulsionTechnology
     *
     * @return integer
     */
    public function getCombustionPropulsionTechnology()
    {
        return $this->combustion_propulsion_technology;
    }

    /**
     * Set impulsionPropulsionTechnology
     *
     * @param integer $impulsionPropulsionTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setImpulsionPropulsionTechnology($impulsionPropulsionTechnology)
    {
        $this->impulsion_propulsion_technology = $impulsionPropulsionTechnology;

        return $this;
    }

    /**
     * Get impulsionPropulsionTechnology
     *
     * @return integer
     */
    public function getImpulsionPropulsionTechnology()
    {
        return $this->impulsion_propulsion_technology;
    }

    /**
     * Set hyperspacePropulsionTechnology
     *
     * @param integer $hyperspacePropulsionTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setHyperspacePropulsionTechnology($hyperspacePropulsionTechnology)
    {
        $this->hyperspace_propulsion_technology = $hyperspacePropulsionTechnology;

        return $this;
    }

    /**
     * Get hyperspacePropulsionTechnology
     *
     * @return integer
     */
    public function getHyperspacePropulsionTechnology()
    {
        return $this->hyperspace_propulsion_technology;
    }

    /**
     * Set spyTechnology
     *
     * @param integer $spyTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setSpyTechnology($spyTechnology)
    {
        $this->spy_technology = $spyTechnology;

        return $this;
    }

    /**
     * Get spyTechnology
     *
     * @return integer
     */
    public function getSpyTechnology()
    {
        return $this->spy_technology;
    }

    /**
     * Set computerTechnology
     *
     * @param integer $computerTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setComputerTechnology($computerTechnology)
    {
        $this->computer_technology = $computerTechnology;

        return $this;
    }

    /**
     * Get computerTechnology
     *
     * @return integer
     */
    public function getComputerTechnology()
    {
        return $this->computer_technology;
    }

    /**
     * Set astrophysicTechnology
     *
     * @param integer $astrophysicTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setAstrophysicTechnology($astrophysicTechnology)
    {
        $this->astrophysic_technology = $astrophysicTechnology;

        return $this;
    }

    /**
     * Get astrophysicTechnology
     *
     * @return integer
     */
    public function getAstrophysicTechnology()
    {
        return $this->astrophysic_technology;
    }

    /**
     * Set researchNetworkTechnology
     *
     * @param integer $researchNetworkTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setResearchNetworkTechnology($researchNetworkTechnology)
    {
        $this->research_network_technology = $researchNetworkTechnology;

        return $this;
    }

    /**
     * Get researchNetworkTechnology
     *
     * @return integer
     */
    public function getResearchNetworkTechnology()
    {
        return $this->research_network_technology;
    }

    /**
     * Set gravitonTechnology
     *
     * @param integer $gravitonTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setGravitonTechnology($gravitonTechnology)
    {
        $this->graviton_technology = $gravitonTechnology;

        return $this;
    }

    /**
     * Get gravitonTechnology
     *
     * @return integer
     */
    public function getGravitonTechnology()
    {
        return $this->graviton_technology;
    }

    /**
     * Set weaponTechnology
     *
     * @param integer $weaponTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setWeaponTechnology($weaponTechnology)
    {
        $this->weapon_technology = $weaponTechnology;

        return $this;
    }

    /**
     * Get weaponTechnology
     *
     * @return integer
     */
    public function getWeaponTechnology()
    {
        return $this->weapon_technology;
    }

    /**
     * Set shieldTechnology
     *
     * @param integer $shieldTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setShieldTechnology($shieldTechnology)
    {
        $this->shield_technology = $shieldTechnology;

        return $this;
    }

    /**
     * Get shieldTechnology
     *
     * @return integer
     */
    public function getShieldTechnology()
    {
        return $this->shield_technology;
    }

    /**
     * Set structureTechnology
     *
     * @param integer $structureTechnology
     *
     * @return PlaneteSpyReport
     */
    public function setStructureTechnology($structureTechnology)
    {
        $this->structure_technology = $structureTechnology;

        return $this;
    }

    /**
     * Get structureTechnology
     *
     * @return integer
     */
    public function getStructureTechnology()
    {
        return $this->structure_technology;
    }

    /**
     * Set missileLauncher
     *
     * @param integer $missileLauncher
     *
     * @return PlaneteSpyReport
     */
    public function setMissileLauncher($missileLauncher)
    {
        $this->missile_launcher = $missileLauncher;

        return $this;
    }

    /**
     * Get missileLauncher
     *
     * @return integer
     */
    public function getMissileLauncher()
    {
        return $this->missile_launcher;
    }

    /**
     * Set lightLaser
     *
     * @param integer $lightLaser
     *
     * @return PlaneteSpyReport
     */
    public function setLightLaser($lightLaser)
    {
        $this->light_laser = $lightLaser;

        return $this;
    }

    /**
     * Get lightLaser
     *
     * @return integer
     */
    public function getLightLaser()
    {
        return $this->light_laser;
    }

    /**
     * Set heavyLaser
     *
     * @param integer $heavyLaser
     *
     * @return PlaneteSpyReport
     */
    public function setHeavyLaser($heavyLaser)
    {
        $this->heavy_laser = $heavyLaser;

        return $this;
    }

    /**
     * Get heavyLaser
     *
     * @return integer
     */
    public function getHeavyLaser()
    {
        return $this->heavy_laser;
    }

    /**
     * Set ionArtillery
     *
     * @param integer $ionArtillery
     *
     * @return PlaneteSpyReport
     */
    public function setIonArtillery($ionArtillery)
    {
        $this->ion_artillery = $ionArtillery;

        return $this;
    }

    /**
     * Get ionArtillery
     *
     * @return integer
     */
    public function getIonArtillery()
    {
        return $this->ion_artillery;
    }

    /**
     * Set gaussCanon
     *
     * @param integer $gaussCanon
     *
     * @return PlaneteSpyReport
     */
    public function setGaussCanon($gaussCanon)
    {
        $this->gauss_canon = $gaussCanon;

        return $this;
    }

    /**
     * Get gaussCanon
     *
     * @return integer
     */
    public function getGaussCanon()
    {
        return $this->gauss_canon;
    }

    /**
     * Set plasmaLauncher
     *
     * @param integer $plasmaLauncher
     *
     * @return PlaneteSpyReport
     */
    public function setPlasmaLauncher($plasmaLauncher)
    {
        $this->plasma_launcher = $plasmaLauncher;

        return $this;
    }

    /**
     * Get plasmaLauncher
     *
     * @return integer
     */
    public function getPlasmaLauncher()
    {
        return $this->plasma_launcher;
    }

    /**
     * Set satellites
     *
     * @param integer $satellites
     *
     * @return PlaneteSpyReport
     */
    public function setSatellites($satellites)
    {
        $this->satellites = $satellites;

        return $this;
    }

    /**
     * Get satellites
     *
     * @return integer
     */
    public function getSatellites()
    {
        return $this->satellites;
    }

    /**
     * Set lightFighter
     *
     * @param integer $lightFighter
     *
     * @return PlaneteSpyReport
     */
    public function setLightFighter($lightFighter)
    {
        $this->light_fighter = $lightFighter;

        return $this;
    }

    /**
     * Get lightFighter
     *
     * @return integer
     */
    public function getLightFighter()
    {
        return $this->light_fighter;
    }

    /**
     * Set heavyFighter
     *
     * @param integer $heavyFighter
     *
     * @return PlaneteSpyReport
     */
    public function setHeavyFighter($heavyFighter)
    {
        $this->heavy_fighter = $heavyFighter;

        return $this;
    }

    /**
     * Get heavyFighter
     *
     * @return integer
     */
    public function getHeavyFighter()
    {
        return $this->heavy_fighter;
    }

    /**
     * Set cruiser
     *
     * @param integer $cruiser
     *
     * @return PlaneteSpyReport
     */
    public function setCruiser($cruiser)
    {
        $this->cruiser = $cruiser;

        return $this;
    }

    /**
     * Get cruiser
     *
     * @return integer
     */
    public function getCruiser()
    {
        return $this->cruiser;
    }

    /**
     * Set battleship
     *
     * @param integer $battleship
     *
     * @return PlaneteSpyReport
     */
    public function setBattleship($battleship)
    {
        $this->battleship = $battleship;

        return $this;
    }

    /**
     * Get battleship
     *
     * @return integer
     */
    public function getBattleship()
    {
        return $this->battleship;
    }

    /**
     * Set tracker
     *
     * @param integer $tracker
     *
     * @return PlaneteSpyReport
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;

        return $this;
    }

    /**
     * Get tracker
     *
     * @return integer
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set bomber
     *
     * @param integer $bomber
     *
     * @return PlaneteSpyReport
     */
    public function setBomber($bomber)
    {
        $this->bomber = $bomber;

        return $this;
    }

    /**
     * Get bomber
     *
     * @return integer
     */
    public function getBomber()
    {
        return $this->bomber;
    }

    /**
     * Set destroyer
     *
     * @param integer $destroyer
     *
     * @return PlaneteSpyReport
     */
    public function setDestroyer($destroyer)
    {
        $this->destroyer = $destroyer;

        return $this;
    }

    /**
     * Get destroyer
     *
     * @return integer
     */
    public function getDestroyer()
    {
        return $this->destroyer;
    }

    /**
     * Set deathStar
     *
     * @param integer $deathStar
     *
     * @return PlaneteSpyReport
     */
    public function setDeathStar($deathStar)
    {
        $this->death_star = $deathStar;

        return $this;
    }

    /**
     * Get deathStar
     *
     * @return integer
     */
    public function getDeathStar()
    {
        return $this->death_star;
    }

    /**
     * Set recycler
     *
     * @param integer $recycler
     *
     * @return PlaneteSpyReport
     */
    public function setRecycler($recycler)
    {
        $this->recycler = $recycler;

        return $this;
    }

    /**
     * Get recycler
     *
     * @return integer
     */
    public function getRecycler()
    {
        return $this->recycler;
    }

    /**
     * Set lightTransporter
     *
     * @param integer $lightTransporter
     *
     * @return PlaneteSpyReport
     */
    public function setLightTransporter($lightTransporter)
    {
        $this->light_transporter = $lightTransporter;

        return $this;
    }

    /**
     * Get lightTransporter
     *
     * @return integer
     */
    public function getLightTransporter()
    {
        return $this->light_transporter;
    }

    /**
     * Set heavyTransporter
     *
     * @param integer $heavyTransporter
     *
     * @return PlaneteSpyReport
     */
    public function setHeavyTransporter($heavyTransporter)
    {
        $this->heavy_transporter = $heavyTransporter;

        return $this;
    }

    /**
     * Get heavyTransporter
     *
     * @return integer
     */
    public function getHeavyTransporter()
    {
        return $this->heavy_transporter;
    }

    /**
     * Set spyProbe
     *
     * @param integer $spyProbe
     *
     * @return PlaneteSpyReport
     */
    public function setSpyProbe($spyProbe)
    {
        $this->spy_probe = $spyProbe;

        return $this;
    }

    /**
     * Get spyProbe
     *
     * @return integer
     */
    public function getSpyProbe()
    {
        return $this->spy_probe;
    }

    /**
     * Set colonisationShip
     *
     * @param integer $colonisationShip
     *
     * @return PlaneteSpyReport
     */
    public function setColonisationShip($colonisationShip)
    {
        $this->colonisation_ship = $colonisationShip;

        return $this;
    }

    /**
     * Get colonisationShip
     *
     * @return integer
     */
    public function getColonisationShip()
    {
        return $this->colonisation_ship;
    }

    /**
     * Set planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     *
     * @return PlaneteSpyReport
     */
    public function setPlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete = null)
    {
        $this->planete = $planete;

        return $this;
    }

    /**
     * Get planete
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Planete
     */
    public function getPlanete()
    {
        return $this->planete;
    }

    /**
     * Set player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     *
     * @return PlaneteSpyReport
     */
    public function setPlayer(\Maesbox\OGInspectorBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set target
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $target
     *
     * @return PlaneteSpyReport
     */
    public function setTarget(\Maesbox\OGInspectorBundle\Entity\Player $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Player
     */
    public function getTarget()
    {
        return $this->target;
    }
}
