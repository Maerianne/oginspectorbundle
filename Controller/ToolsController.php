<?php

namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ToolsController extends Controller 
{
    public function indexAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Tools:index.html.twig', 
                array());
    }
    
    public function ogiscanAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Tools:ogiscan.html.twig', 
                array());
    }
    
    public function xtenseAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Tools:xtense.html.twig', 
                array());
    }
}