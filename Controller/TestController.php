<?php
namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestController extends Controller 
{
    public function indexAction(Request $request)
    {
        $ogapi = $this->get('maesbox.oginspector.api.ogame');
        
        $serverlist = $ogapi->getServerList('fr');
        $serverinfo = $ogapi->getServerInfos('fr', 126);
        
        $data = array(
            "serverlist" => $serverlist,
            "serverinfo" => $serverinfo,
        );
        
        if(is_array($data)){
            return new JsonResponse($data);
        }
        
        
        return new Response($data);
    }
}