<?php
namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Maesbox\OGInspectorBundle\Controller\BaseController;

class ProfileController extends BaseController
{
    
    public function indexAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Player:index.html.twig', 
                array());
    }
    
   
    public function playerAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Player:player.html.twig', 
                array());
    }
    
    
    public function empireAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Player:empire.html.twig', 
                array());
    }
}