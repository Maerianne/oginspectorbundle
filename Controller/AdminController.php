<?php

namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Maesbox\OGInspectorBundle\Entity\Univers;
use Maesbox\OGInspectorBundle\Form\Type\UniversType;

class AdminController extends Controller 
{
    public function indexAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        
        
        return $this->render(
                "MaesboxOGInspectorBundle:Admin:index.html.twig",
                array(
                    
                ));
    }
        
    public function universAdminAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        
        $univers = new Univers();
        
        $universform = $this->createForm(UniversType::class, $univers);
        
        $universform->handleRequest($request);

        if ($universform->isSubmitted() && $universform->isValid()) {

        }
        
        return $this->render(
                "MaesboxOGInspectorBundle:Admin:univers.html.twig",
                array(
                    "universForm" => $universform->createView()
                ));
    }
    
    public function updateUniversAdminAction(Request $request, $id)
    {
        $manager = $this->getDoctrine()->getManager();
        
        $univers = $manager->getRepository('MaesboxOGInspector:Univers')->find($id);
        
        if(!$univers) {
            throw $this->createNotFoundException('The univers does not exist');
        }
    }
    
    public function deleteUniversAdminAction(Request $request, $id)
    {
        $manager = $this->getDoctrine()->getManager();
        
        $univers = $manager->getRepository('MaesboxOGInspector:Univers')->find($id);
        
        if(!$univers) {
            throw $this->createNotFoundException('The univers does not exist');
        }
    }
}