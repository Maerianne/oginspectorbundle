<?php

namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Maesbox\OGInspectorBundle\Entity\Univers;

class BaseController extends Controller 
{
    protected $univers;
    
    
    /**
     * @return Univers
     */
    public function getUnivers(){
        return $this->univers;
    }
    
    /**
     * @param Univers $univers
     * @return Controller
     */
    public function setUnivers(Univers $univers){
        $this->univers = $univers;
        return $this;
    }
}