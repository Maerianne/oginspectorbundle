<?php
namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class MainController extends Controller 
{
    public function indexAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Home:index.html.twig', 
                array());
    }
}