<?php
namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Maesbox\OGInspectorBundle\Controller\BaseController;

class AllianceController extends BaseController
{
    
    public function indexAction(Request $request)
    {
        return $this->render(
                'MaesboxOGInspectorBundle:Alliance:index.html.twig', 
                array());
    }
    
}
   