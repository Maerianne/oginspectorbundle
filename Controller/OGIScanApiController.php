<?php

namespace Maesbox\OGInspectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class OGIScanApiController extends Controller 
{
    /**
     * @ApiDoc(
     *      description="Api update planet overview",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *         
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function planetOverviewAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update planet buildings",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function planetBuildingsAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update planet installations",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function planetInstallationsAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update planet defenses",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function planetDefensesAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update fleet defenses",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function planetFleetAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update moon overview",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *         
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function moonOverviewAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update moon buildings",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function moonBuildingsAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update moon installations",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function moonInstallationsAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update moon defenses",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function moonDefensesAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update moon fleet",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function moonFleetAction(Request $request)
    {
        return new JsonResponse();
    }
    
    /**
     * @ApiDoc(
     *      description="Api update player research",
     *      input="",
     *      statusCodes={
     *         200="Returned when successful"
     *     })
     * @param Request $request
     * @return JsonResponse Description
     */
    public function playerResearchAction(Request $request)
    {
        return new JsonResponse();
    }
}