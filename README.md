MaesboxOGInspectorBundle
====================
[![Latest Stable Version](https://poser.pugx.org/maesbox/oginspectorbundle/v/stable.svg)](https://packagist.org/packages/maesbox/oginspectorbundle) [![Total Downloads](https://poser.pugx.org/maesbox/oginspectorbundle/downloads.svg)](https://packagist.org/packages/maesbox/oginspectorbundle) [![Latest Unstable Version](https://poser.pugx.org/maesbox/oginspectorbundle/v/unstable.svg)](https://packagist.org/packages/maesbox/oginspectorbundle) [![License](https://poser.pugx.org/maesbox/oginspectorbundle/license.svg)](https://packagist.org/packages/maesbox/oginspectorbundle)

Présentation 
--------------------

Ce bundle fournit différents outils pour l'espionnage, la collecte d'informations pour les alliances sur le jeux Ogame

Installation
--------------------

- installation avec composer

*dans le composer.json*
~~~~

    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "jquery/jquery",
                "version": "1.11.1",
                "dist": {
                    "url": "http://code.jquery.com/jquery-1.11.1.js",
                    "type": "file"
                }
            }
        }
    ],
    ...
    "require": {
        ...
        "maesbox/oginspectorbundle": "dev-master",
        "jquery/jquery":  "1.11.*"
        ...
    },

~~~~


Configuration
--------------------

*dans le config.yml*
~~~~

    maesbox_og_inspector:
        class:
            user: /* you user class */

~~~~