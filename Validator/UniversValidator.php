<?php
namespace Maesbox\OGInspectorBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Doctrine\ORM\EntityManager;

use Maesbox\OGInspectorBundle\Services\ApiOgame;

class UniversValidator extends ConstraintValidator
{
    protected $manager;
    
    protected $api;

    public function __construct(EntityManager $manager, ApiOgame $api)
    {
        parent::__contruct();
        $this->setManager($manager);
        $this->setApi($api);
    }

    /**
     * @param type $univers
     * @param Constraint $constraint
     */
    public function validate($univers, Constraint $constraint)
    {
        $db_univers = $this->getManager()->getRepository("MaesboxOginspector:Univers")->findOneBy(array());
        
        if($db_univers){
            $this->context->buildViolation($constraint->message)
                ->atPath('language')
                ->addViolation();
        }
        
        if ($univers->getLanguage() != $univers->getNumber()) {
            // If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->atPath('foo')
                ->addViolation();

            // If you're using the old 2.4 validation API
            /*
            $this->context->addViolationAt(
                'foo',
                $constraint->message,
                array(),
                null
            );
            */
        }
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    /**
     * @param ApiOgame $api
     * @return UniversValidator
     */
    public function setApi(ApiOgame $api){
        $this->api = $api;
        return $this;
    }
    
    /**
     * @param EntityManager $manager
     * @return UniversValidator
     */
    public function setManager(EntityManager $manager){
        $this->manager = $manager;
        return $this;
    }
    
    /**
     * @return OgameApi
     */
    public function getApi(){
        return $this->api;
    }
    
    /**
     * @return EntityManager
     */
    public function getManager(){
        return $this->manager;
    }
}