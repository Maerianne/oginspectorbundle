<?php

namespace Maesbox\OGInspectorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class isValidUnivers extends Constraint
{
    public $message = 'The string "%string%" contains an illegal character: it can only contain letters or numbers.';
    
    
    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'alias_name';
    }
}